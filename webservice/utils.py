import os
import json
import pickle
import re
from collections import defaultdict, namedtuple
import datetime
import random
import html
from fuzzywuzzy import fuzz
import requests

class DataSource():
    def __init__(self, data_dir="./"):
        with open(f"{data_dir}/entity_terms.json", 'rb') as f:
            self.entity_terms = json.load(f)
    
        with open(f"{data_dir}/pred_matches.pkl", 'rb') as f:
            self.pred_matches = pickle.load(f)
    
        with open(f'{data_dir}/spl_texts.json', 'rt') as f: 
            self.spl_texts = json.load(f)
        
        with open(f'{data_dir}/spl2mrn.json', 'rt') as f:
            self.spl2mrn = json.load(f)
        self.mrn2spl = defaultdict(set)
        for k,vs in self.spl2mrn.items():
            for v in vs:
                self.mrn2spl[v].add(k)
        with open(f'{data_dir}/mrn2name.json', 'rt') as f:
            self.mrn2name = json.load(f)
        self.mrn2name = {int(k):v for k,v in self.mrn2name.items() if not k == 'null'}
        
        with open(f'{data_dir}/dm_chembl_data_codes.json','rt') as f: 
            self.dm_chembl_data = json.load(f)
        with open(f'{data_dir}/dd_chembl_data_codes.json','rt') as f: 
            self.dd_chembl_data = json.load(f)
        
        with open(f"{data_dir}/equivalent_index.json", 'rt') as f:
            self.equivalent_index = json.load(f)
        self.r_equivalent_index = {}
        for k,vs in self.equivalent_index.items():
            for v in vs:
                self.r_equivalent_index[v] = k
        with open(f"{data_dir}/disease_hierarch_index.json", 'rt') as f:
            self.disease_hierarchy_index = json.load(f)
    
    def pick_top_name(self, onto_code, target_strings):
        if not onto_code in self.entity_terms:
            return None
        
        scores = {}
        for pred,terms in self.entity_terms[onto_code].items():
            for term in terms:
                d = []
                for target_string in target_strings:
                    d.append(fuzz.ratio(term.lower(), target_string.lower()))
                scores[term] = max(d)
            
        return sorted(scores.items(), key=lambda x:x[1], reverse=True)[0][0]
    
    def get_all(self):
        return set(self.pred_matches.keys())
    
    def group_entities(self, terms_entities):        
        # find groups to merge
        merge_dict = defaultdict(set)
        rev_dict = defaultdict(set)
        for term, entities in terms_entities.items():
            for entity in entities:
                rev_dict[entity].add(term)
                merge_dict[term].update(rev_dict[entity])
        
        # make sure all groups "know about" each other
        for term, to_merge in merge_dict.items():
            for t in to_merge:
                if t == term: 
                    continue
                merge_dict[t].update(to_merge)
        
        # compile list of distinct groups
        merge_groups = []
        used_groups = set()
        for term, to_merge in merge_dict.items():
            if term in used_groups:
                continue
            merge_groups.append(list(to_merge))
            used_groups.update(to_merge)
        
        # group them
        grouped_terms_entities = {}
        for group in merge_groups:
            k = tuple(group)
            v = set()
            for term in group:
                v.update(terms_entities[term])
            grouped_terms_entities[k] = v
        
        return grouped_terms_entities
    
    def get_distance(self, iri1, iri2):
        if iri1 in self.r_equivalent_index:
            iri1_eq = set(self.equivalent_index[self.r_equivalent_index[iri1]])
        else:
            iri1_eq = {iri1}
        if iri2 in self.r_equivalent_index:
            iri2_eq = set(self.equivalent_index[self.r_equivalent_index[iri2]])
        else:
            iri2_eq = {iri2}

        if iri1_eq & iri2_eq:
            return 0

        distances = set()
        for iri in iri1_eq:
            if iri in self.disease_hierarchy_index:
                distances.update({d for k,d in self.disease_hierarchy_index[iri].items() if k in iri2_eq})
        for iri in iri2_eq:
            if iri in self.disease_hierarchy_index:
                distances.update({d for k,d in self.disease_hierarchy_index[iri].items() if k in iri1_eq})

        if distances:
            return min(distances)
    
    def match_prediction(self, spl_set_id, pred_codes, distance_threshold=1):
        chembl_codes = set()
        set_ids = {spl_set_id}
        if spl_set_id in self.spl2mrn:
            for mrn in self.spl2mrn[spl_set_id]:
                set_ids.update(self.mrn2spl[mrn])
                if mrn in self.dd_chembl_data:
                    for k,vs in self.dd_chembl_data[mrn].items():
                        chembl_codes.update(vs)  # codes from DRUGBASE.DRUG_DISEASE
        
        for set_id in set_ids:
            if set_id in self.dm_chembl_data:
                for k,vs in self.dm_chembl_data[set_id].items():
                    chembl_codes.update(vs)  # codes from DRUGBASE.DAILYMED_DISEASES_TMP

        distances = defaultdict(set)
        for pred_iri in pred_codes:
            for chembl_iri in chembl_codes:
                d = self.get_distance(pred_iri, chembl_iri)
                if not d is None:
                    distances[d].add((pred_iri, chembl_iri))
        
        if distances:
            d = min(distances.keys())
            if d <= distance_threshold:
                return {(p,d,c) for p,c in distances[d]}
        else:
            return set()
    
    def get_annotations_data(self, set_id, html_data):
        def search_for_term(term, text):
            scores = {}
            for i in range(len(text)-len(term)):
                scores[(i,i+len(term))] = fuzz.ratio(text[i:i+len(term)], term)/100
            span, score = sorted(scores.items(), key=lambda x:(1-x[1],x[0]))[0]
            return span, score
        
        def char_to_html_span(sel_span):
            node_span = tuple([doc2node_char[l] for l in sel_span])
            html_span = tuple([(node2html_loc[n_id], l) for (n_id,l) in node_span])
            return html_span
        
        doc_text, node2html_loc, doc2node_char = html_data
        doc_text = doc_text.lower()
        
        pred_data = namedtuple("PredictionData", "doc_text, preds, codes, doc_span, html_span, chembl_consistency, max_score")
        for preds,codes in self.group_entities({k[0]:v for k,v in self.pred_matches[set_id].items()}).items():
            html_span = None
            span_scores = defaultdict(set)
            for pred in preds:
                sel_span, score = search_for_term(pred, doc_text)
                span_scores[score].add(sel_span)
            if span_scores:
                max_score = max(span_scores.keys())
                doc_span = sorted(span_scores[max_score])[0]
#                 if max_score < 0.7:
#                     raise Exception(f"max_score < 0.7 ({max_score}). {preds} -> {doc_text[doc_span[0]:doc_span[1]]}")
                html_span = char_to_html_span(doc_span)

            chembl_consistency = self.match_prediction(set_id, codes)
        
            yield pred_data(doc_text, preds, codes, doc_span, html_span, chembl_consistency, max_score)
    
    def print_annotations(self, data):
        def html_loc2path(loc):
            return '/' + '/'.join([f'{tag}[{n+1}]' for tag,n in loc])
            
        for d in data:
            print("; ".join(d.preds))
            for code in d.codes:
                print(f'    {code} ({self.pick_top_name(code,d.preds)})')
            
            if d.html_span:
                print('')
                print(f'    {html_loc2path(d.html_span[0][0])}')
                print(f'    {html_loc2path(d.html_span[1][0])}')
            
            if d.chembl_consistency:
                print('')
                for pred_iri, distance, chembl_iri in d.chembl_consistency:
                    print(f'    {pred_iri} ({self.pick_top_name(pred_iri,d.preds)}) -[{distance}]-> {chembl_iri} ({self.pick_top_name(chembl_iri,d.preds)})')
                    break
            print('')
            print('')
    
    def gen_hypothesis_annotations(self, set_id, html_data, match_score_threshold=0.8, base_url='http://localhost:8007'):
        def html_loc2path(loc):
            return '/' + '/'.join([f'{tag}[{n+1}]' for tag,n in loc])
        
        for data in self.get_annotations_data(set_id, html_data):
            l = 32
            text = data.doc_text[data.doc_span[0]:data.doc_span[1]]
            prefix = data.doc_text[data.doc_span[0]-1-l:data.doc_span[0]-1]
            suffix = data.doc_text[data.doc_span[1]+1:data.doc_span[1]+1+l]

            uri = f"{base_url}/{set_id}.htm"
            
            message = ""
                
            message += "; ".join(data.preds) + "\n"
            
            if data.max_score < match_score_threshold:
                message += f"\n**Not found in document** (score: {data.max_score}, best match: '{data.doc_text[data.doc_span[0]:data.doc_span[1]]}')\n"
                
            for code in data.codes:
                message += f'* {code} ({self.pick_top_name(code,data.preds)})\n'
            
            if data.chembl_consistency:
                message += "\nChEMBL evidence\n"
                for pred_iri, distance, chembl_iri in data.chembl_consistency:
                    message += f'* {pred_iri} ({self.pick_top_name(pred_iri,data.preds)}) -[{distance}]-> {chembl_iri} ({self.pick_top_name(chembl_iri,data.preds)})\n'
                    break  # only include one piece of evidence
            
            tags = ['prediction']
            if not data.chembl_consistency:
                tags.append('needs checking')
            if data.max_score < match_score_threshold:
                tags.append('not in document')
                
            annotation = {
                "uri": uri,
                "document": {"title": [set_id]},
                "text": message,
                "tags": tags,
            }
            
            if data.max_score >= match_score_threshold:
                annotation["target"] = [{
                    'selector': [
                        {
                            "startContainer": html_loc2path(data.html_span[0][0]),
                            "endContainer": html_loc2path(data.html_span[1][0]),
                            'startOffset': data.doc_span[0],
                            'endOffset': data.doc_span[1],
                            'type': 'RangeSelector',
                        },
                        {
                            'exact': text,
#                             'prefix': prefix,
#                             'suffix': suffix,
                            'type': 'TextQuoteSelector'
                        }],
                    'source': uri
                }]

            yield annotation
    
    def get_false_negatives(self, set_id, distance_threshold=1):
        mismatches = set()
        
        dm_chembl_codes = set()
        for k,vs in self.dm_chembl_data[set_id].items():
            dm_chembl_codes.update(vs)
        pred_codes = self.pred_matches[set_id]

        for chembl_iri in dm_chembl_codes:
            distances = set()
            for (pred, pred_str),pred_codes in self.pred_matches[set_id].items():
                for pred_iri in pred_codes:
                    d = self.get_distance(pred_iri, chembl_iri)
                    if not d is None:
                        distances.add(d)
            min_distance = None
            if distances:
                if min(distances) <= distance_threshold:
                    min_distance = min(distances)
            if min_distance is None:
                mismatches.add(chembl_iri)
                
        return mismatches
