# import sqlite3
import json
import pickle
import datetime
# from http.server import HTTPServer, BaseHTTPRequestHandler
import re
import requests
import os
import sys
import random

import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, DateTime, MetaData, ForeignKey
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import func

class HypothesisHandler():
    
    source_date_format="%Y-%m-%dT%H:%M:%S.%f%z"
    target_date_format="%Y-%m-%d %H:%M:%S"
    
    def __init__(self, group='mN1dodAW', limit=20, token='Bearer 6879-l91CDYdk-OqBW0ZfhMg8t0QGH_DdH1RcTmT5LlNx2bk', hypothesis_api_url="https://api.hypothes.is/api"):
        self.group = group
        self.limit = limit
        self.token = token
        self.hypothesis_api_url = hypothesis_api_url
    
    def norm_datetime(self, s):
        dt = datetime.datetime.strptime(s, self.source_date_format)
        dt_str = datetime.datetime.strftime(dt, self.target_date_format)
        return dt_str
    
    def request_annotations(self, uri=None, user=None):
        rows = []
        last = None
        while True:
            r = requests.get(url=f'{self.hypothesis_api_url}/search',
                             params={'search_after': last,
                                     'limit': self.limit,
                                     'sort': 'created',
                                     'user': user,
                                     'group': self.group,
                                     'uri': uri},
                     headers={'Authorization': self.token})
            r = r.json()
            rows += r['rows']
            if len(r['rows']) < self.limit: 
                break
            last = rows[-1]['created']

        return rows

    def process_annotations(self, rows):
        return rows
                    
    def get_annotations(self, uri):
        rows = self.request_annotations(uri=uri, user=None)
        return self.process_annotations(rows)
    
class TweetHypothesisHandler(HypothesisHandler):
    
    def process_annotations(self, rows):
        for r in rows:
            d = re.findall('^.*?/([a-zA-Z0-9]+)$', r['uri'])
            if len(d):
                twitter_id = d[0]
                yield {'hypothesis_id': r['id'], 'date': datetime.datetime.strptime(r['updated'],self.source_date_format), 
                       'user': r['user'], 'annotation': r['text'], 'tags': r['tags'], 
                       'uri': r['uri'], 'twitter_id': twitter_id}

class TweetDatabaseHandler():
    
    date_format = "%Y-%m-%d %H:%M:%S"
    
    Base = declarative_base()
    
    class Tweet(Base):
        __tablename__ = "tweets"

        id = Column(Integer, primary_key=True)
        twitter_id = Column(String(32), index=True, unique=True)
        user_name = Column(String(50), index=True)
        text = Column(String(2048))
        url = Column(String(1024))
        date = Column(DateTime, index=True)
        annotation_priority = Column(Integer, index=True)

        tweet_termite_annotations = relationship("Tweet_Termite_Annotation", back_populates="tweet", uselist=True, cascade="all, delete, delete-orphan")
        annotations = relationship("Annotation", back_populates="tweet", cascade="all, delete, delete-orphan")

    class Tweet_Termite_Annotation(Base):
        __tablename__ = "tweet_termite_annotations"

        id = Column(Integer, primary_key=True)
        tweet_id = Column(Integer, ForeignKey('tweets.id'), index=True)
        tweet = relationship("Tweet", back_populates="tweet_termite_annotations")

        entity_type = Column(String(32))
        entity_id = Column(String(64))
        entity_name = Column(String(128))
        match_syn = Column(String(128))
        match_pos = Column(String(64))
        score = Column(Integer)
        nonambig = Column(Integer)
        meshtree = Column(String(1024))
        
    class Annotation(Base):
        __tablename__ = "annotations"

        id = Column(Integer, primary_key=True)
        hypothesis_id = Column(String(32), index=True)
        tweet_id = Column(Integer, ForeignKey('tweets.id'), index=True)
        tweet = relationship("Tweet", back_populates="annotations")

        user = Column(String(64), index=True)
        annotation = Column(String(1024))
        tags = Column(String(1024))
        date = Column(DateTime, index=True)
    
    def __init__(self, db_path="./clinical_trials_annotations.sqlite"):
        self.db_path = db_path
        self.engine = sqlalchemy.create_engine(self.db_path)
        self.SessionMaker = sessionmaker(bind=self.engine)
        
    def create(self):
        self.Base.metadata.create_all(self.engine)
        
    def drop_all(self):
        self.Base.metadata.drop_all(self.engine)
    
    def rec_vars(self,d):
        if isinstance(d, (self.Tweet, self.Annotation, self.Tweet_Termite_Annotation)):
            d = vars(d)
            
        if isinstance(d, datetime.datetime):
            d = datetime.datetime.strftime(d, self.date_format)

        if isinstance(d, dict):
            r = {}
            for k,v in d.items():
                if k == '_sa_instance_state': 
                    continue
                r[k] = self.rec_vars(v)
            return r

        if isinstance(d, (list, tuple)):
            r = []
            for i in d:
                r.append(self.rec_vars(i))
            return r

        return d
    
    def norm_datetime(self, s):
        dt_str = datetime.datetime.strftime(s, self.date_format)
        dt = datetime.datetime.strptime(dt_str, self.date_format)
        return dt
    
    def nct_id2trial_id(self, nct_id):
        session = self.SessionMaker()
        data = session.query(self.Trial.id)\
                      .filter(self.Trial.nct_id==nct_id)\
                      .scalar()
        session.close()
        return data
        
    def twitter_id2tweet_id(self, twitter_id):
        session = self.SessionMaker()
        data = session.query(self.Tweet.id)\
                      .filter(self.Tweet.twitter_id==twitter_id)\
                      .scalar()
        session.close()
        return data
    
    def str2date(self, date_string):
        if date_string is None: 
            return None
        else:
            return datetime.datetime.strptime(date_string, self.date_format)
    
    def get_tweet_termite_annotations(self, tweet_id):
        session = self.SessionMaker()
        data = session.query(self.Tweet_Termite_Annotation)\
                      .filter(self.Tweet_Termite_Annotation.tweet_id==tweet_id)\
                      .all()
        session.close()

        data = [vars(d) for d in data]
        return data
        
    def get_tweet_data(self, tweet_id):
        
        session = self.SessionMaker()
        tweet = session.query(self.Tweet)\
                       .get(tweet_id)
        tweet.tweet_termite_annotations
        return_data = self.rec_vars(tweet)
        session.close()
        
        return return_data
    
    def get_hypothesis_annotations(self, tweet_id):
        session = self.SessionMaker()
        data = session.query(self.Annotation)\
                      .get(tweet_id)\
                      .all()
        session.close()
        
        data = [vars(d) for d in data]

        eval_cols = ['tags']
        for i,d in enumerate(data):
            for k in eval_cols:
                data[i][k] = eval(d[k])
            
        return data
    
    def add_annotation(self, new_annotation):
        new_annotation = self.Annotation(hypothesis_id=new_annotation['hypothesis_id'], 
                                         tweet_id=new_annotation['tweet_id'], 
                                         user=new_annotation['user'], 
                                         annotation=new_annotation['annotation'], 
                                         tags=repr(new_annotation['tags']), 
                                         date=new_annotation['date'])
        
        session = self.SessionMaker()
        session.add(new_annotation)
        session.commit()
        session.close()
        
        return new_annotation
    
    def delete_annotation(self, annotation_id):
        session = self.SessionMaker()
        annotation = session.query(self.Annotation)\
                            .get(annotation_id)
        session.delete(annotation)
        session.commit()
        session.close()
    
    def update_annotation(self, new_annotation, annotation_id):
        session = self.SessionMaker()
        old_annotation_obj = session.query(self.Annotation)\
                                .get(annotation_id)
        old_annotation = vars(old_annotation_obj)
        
        date_cols = ['date']
        for k in date_cols:
            old_annotation[k] = self.norm_datetime(old_annotation[k])
            new_annotation[k] = self.norm_datetime(new_annotation[k])
        
        cols = ['user', 'annotation', 'tags', 'date']
        old_annotation['tags'] = sorted(eval(old_annotation['tags']))
        new_annotation['tags'] = sorted(new_annotation['tags'])
        
        update = not all([old_annotation[k]==new_annotation[k] for k in cols])
        
        if update:
#             session.query(self.Annotation).filter(self.Annotation.id==annotation_id)\
#                                           .update({self.Annotation.user: new_annotation['user'],
#                                                    self.Annotation.annotation: new_annotation['annotation'],
#                                                    self.Annotation.tags: repr(new_annotation['tags']),
#                                                    self.Annotation.date: new_annotation['date']})
            old_annotation_obj.user = new_annotation['user']
            old_annotation_obj.annotation = new_annotation['annotation']
            old_annotation_obj.tags = repr(new_annotation['tags'])
            old_annotation_obj.date = new_annotation['date']
            
            session.commit()
        
        session.close()
            
        return update
    
    def get_record(self, twitter_id):
        session = self.SessionMaker()
        record = session.query(self.Tweet)\
                        .filter(self.Tweet.twitter_id==twitter_id)\
                        .one()
        record.tweet_termite_annotations
        record.annotations
        record_data = self.rec_vars(record)
        session.close()
        
        return record_data
    
    def get_all_records(self):
        data = {}
        session = self.SessionMaker()
        for tweet, annotation_id in session.query(self.Tweet, self.Annotation.id).outerjoin(self.Annotation, self.Tweet.id==self.Annotation.tweet_id).all():
            if (tweet.twitter_id, tweet.annotation_priority) in data:
                data[(tweet.twitter_id, tweet.annotation_priority)] += 1
            else:
                data[(tweet.twitter_id, tweet.annotation_priority)] = 1 if annotation_id else 0
        session.close()
                                      
        return [{'twitter_id': twitter_id, 'annotation_priority': annotation_priority, 'annotation_count': annotation_count} for (twitter_id, annotation_priority), annotation_count in data.items()]
    
    def get_next_record(self):
        all_records = self.get_all_records()
        grouped_potential_records = {}
        for r in all_records:
            if r['annotation_count'] > 0: 
                continue
                
            if r['annotation_priority'] in grouped_potential_records:
                grouped_potential_records[r['annotation_priority']].append(r['twitter_id'])
            else:
                grouped_potential_records[r['annotation_priority']] = [r['twitter_id']]
                
        max_annotation_priority = max(list(grouped_potential_records.keys()))
        
        twitter_id = random.sample(grouped_potential_records[max_annotation_priority], 1)[0]
        return {'twitter_id': twitter_id}
    
    def update_annotations(self, twitter_id, h_annotations):
        num_annotations = len(h_annotations)
        num_updated = 0
        num_added = 0
        num_deleted = 0
        
        # get existing annotations
        tweet_data = self.get_record(twitter_id)
        db_annotations = tweet_data['annotations']
        tweet_id = tweet_data['id']
        db_annotations = {d['hypothesis_id']:d for d in db_annotations}
        
        h_annotations = {d['hypothesis_id']:d for d in h_annotations}
        for k in h_annotations.keys():
            h_annotations[k]['tweet_id'] = tweet_id
        
        delete = set(db_annotations.keys()) - set(h_annotations.keys())
        for k in delete:
            self.delete_annotation(db_annotations[k]['id'])
            num_deleted += 1
            
        add = set(h_annotations.keys()) - set(db_annotations.keys())
        for k in add:
            self.add_annotation(h_annotations[k])
            num_added += 1
            
        update = set(h_annotations.keys()) & set(db_annotations.keys())
        for k in update:
            if self.update_annotation(h_annotations[k], db_annotations[k]['id']):
                num_updated += 1
            
        return {'annotations': num_annotations, 'updated': num_updated, 'added': num_added, 'deleted': num_deleted}

# class TweetDatabaseHandler():
#     def __init__(self, db_path="./clinical_trials_annotations.sqlite"):
#         self.db_path = db_path
#         self.date_format = "%Y-%m-%d %H:%M:%S"
        
#     def create(self):
#         with sqlite3.connect(self.db_path) as conn:
#             conn.execute("create table tweet(id INTEGER, twitter_id TEXT, user_name TEXT, text TEXT, url TEXT, date TEXT, annotation_priority INTEGER, primary key(id))")
#             conn.execute("create table tweet_termite_annotation(id INTEGER, tweet_id INTEGER, entity_type TEXT, entity_id TEXT, entity_name TEXT, \
#                                                                 match_syn TEXT, match_pos TEXT, score INTEGER, nonambig INTEGER, meshtree TEXT, \
#                                                                 primary key(id), foreign key(tweet_id) references tweet(id))")
#             conn.execute("create table annotation(id INTEGER, hypothesis_id TEXT, tweet_id INTEGER, user TEXT, annotation TEXT, tags TEXT, date TEXT, \
#                           foreign key(tweet_id) references tweet(id), primary key(id))")

#             conn.execute('create unique index tweet_twitter_id_index ON tweet(twitter_id)')
#             conn.execute('create index tweet_annotation_priority_index ON tweet(annotation_priority)')
#             conn.execute('create index tweet_termite_annotation_tweet_id_index ON tweet_termite_annotation(tweet_id)')

#             conn.execute('create unique index annotation_hypothesis_id_index ON annotation(hypothesis_id)')
#             conn.execute('create index annotation_tweet_id_index ON annotation(tweet_id)')
#             conn.execute('create index annotation_user_index ON annotation(user)')

#             conn.commit()
    
#     def nct_id2trial_id(self, nct_id):
#         with sqlite3.connect(self.db_path) as conn:
#             data = next(conn.execute("select id from trial where nct_id=?", (nct_id,)))[0]
#         return data
        
#     def twitter_id2tweet_id(self, twitter_id):
#         with sqlite3.connect(self.db_path) as conn:
#             data = next(conn.execute("select id from tweet where twitter_id=?", (twitter_id,)))[0]
#         return data
    
#     def str2date(self, date_string):
#         if date_string is None: 
#             return None
#         else:
#             return datetime.datetime.strptime(date_string, self.date_format)
    
#     def get_tweet_termite_annotations(self, tweet_id):
#         with sqlite3.connect(self.db_path) as conn:
#             cols = ["entity_type", "entity_id", "entity_name", "match_syn", "match_pos", "score", "nonambig", "meshtree"]
#             cols_str = ",".join(cols)
#             data = list(conn.execute(f"select {cols_str} from tweet_termite_annotation where tweet_id=?", (tweet_id,)))
#             data = [{cols[i]:v for i,v in enumerate(d)} for d in data]
        
#         return data
        
#     def get_tweet_data(self, tweet_id):
#         with sqlite3.connect(self.db_path) as conn:
#             cols = ["twitter_id", "user_name", "text", "url", "date", "annotation_priority"]
#             cols_str = ",".join(cols)
#             data = next(conn.execute(f"select {cols_str} from tweet where id=?", (tweet_id,)))
#             data = {cols[i]:v for i,v in enumerate(data)}

#             date_cols = ['date']
#             for k in date_cols:
#                 data[k] = self.str2date(data[k])

#             data['termite_annotations'] = self.get_tweet_termite_annotations(tweet_id)
            
#         return data
    
#     def get_hypothesis_annotations(self, tweet_id):
#         with sqlite3.connect(self.db_path) as conn:
#             cols = ['id', 'hypothesis_id', 'user', 'annotation', 'tags', 'date']
#             cols_str = ",".join(cols)
#             data = list(conn.execute(f"select {cols_str} from annotation where tweet_id=?", (tweet_id,)))
#             data = [{cols[i]:v for i,v in enumerate(d)} for d in data]

#             date_cols = ['date']
#             for i,d in enumerate(data):
#                 for k in date_cols:
#                     data[i][k] = self.str2date(d[k])
                    
#             eval_cols = ['tags']
#             for i,d in enumerate(data):
#                 for k in eval_cols:
#                     data[i][k] = eval(d[k])
            
#         return data
    
#     def add_annotation(self, new_annotation):
#         with sqlite3.connect(self.db_path) as conn:
#             conn.execute("insert into annotation(hypothesis_id, tweet_id, user, annotation, tags, date) values (?,?,?,?,?,?)", \
#                          (new_annotation['hypothesis_id'], new_annotation['tweet_id'], new_annotation['user'], new_annotation['annotation'], repr(new_annotation['tags']), new_annotation['date']))
    
#     def delete_annotation(self, annotation_id):
#         with sqlite3.connect(self.db_path) as conn:
#             conn.execute("delete from annotation where id=?", (annotation_id,))
    
#     def update_annotation(self, new_annotation, annotation_id):
#         with sqlite3.connect(self.db_path) as conn:
#             cols = ['user', 'annotation', 'tags']
#             cols_str = ",".join(cols)
#             data = next(conn.execute(f"select {cols_str} from annotation where id=?", (annotation_id,)))
#             data = {cols[i]:v for i,v in enumerate(data)}
#             data['tags'] = eval(data['tags'])
        
#         if all([v==new_annotation[k] for k,v in data.items()]):  # check if any values are different, return True if they are, else change the values
#             return False
#         else:
#             with sqlite3.connect(self.db_path) as conn:
#                 conn.execute(f"update annotation set user=?, annotation=?, tags=?, date=? where id=?", (new_annotation['user'], new_annotation['annotation'], repr(new_annotation['tags']), new_annotation['date'], annotation_id))
#             return True
    
#     def get_record(self, twitter_id):
#         tweet_id = self.twitter_id2tweet_id(twitter_id)
#         tweet_data = self.get_tweet_data(tweet_id)
#         annotation_data = self.get_hypothesis_annotations(tweet_id)
        
#         return {'tweet_id': tweet_id, 'tweet_data': tweet_data, 'annotation_data': annotation_data}
    
#     def get_all_records(self):
#         with sqlite3.connect(self.db_path) as conn:
#             data = {}
#             for twitter_id, annotation_priority, annotation_id in conn.execute("select twitter_id, annotation_priority, annotation.id\
#                                                                    from tweet left join annotation on tweet.id = annotation.tweet_id"):
#                 if (twitter_id, annotation_priority) in data:
#                     data[(twitter_id, annotation_priority)] += 1
#                 else:
#                     if annotation_id:
#                         data[(twitter_id, annotation_priority)] = 1
#                     else:
#                         data[(twitter_id, annotation_priority)] = 0
                                      
#         return [{'twitter_id': twitter_id, 'annotation_priority': annotation_priority, 'annotation_count': annotation_count} for (twitter_id, annotation_priority), annotation_count in data.items()]
    
#     def get_next_record(self):
#         all_records = self.get_all_records()
#         grouped_potential_records = {}
#         for r in all_records:
#             if r['annotation_count'] > 0: 
#                 continue
                
#             if r['annotation_priority'] in grouped_potential_records:
#                 grouped_potential_records[r['annotation_priority']].append(r['twitter_id'])
#             else:
#                 grouped_potential_records[r['annotation_priority']] = [r['twitter_id']]
                
#         max_annotation_priority = max(list(grouped_potential_records.keys()))
        
#         twitter_id = random.sample(grouped_potential_records[max_annotation_priority], 1)[0]
#         return {'twitter_id': twitter_id}
    
#     def update_annotations(self, twitter_id, h_annotations):
#         num_annotations = len(h_annotations)
#         num_updated = 0
#         num_added = 0
#         num_deleted = 0
        
#         # get existing annotations
#         tweet_data = self.get_record(twitter_id)
#         db_annotations = tweet_data['annotation_data']
#         tweet_id = tweet_data['tweet_id']
#         db_annotations = {d['hypothesis_id']:d for d in db_annotations}
        
#         h_annotations = {d['hypothesis_id']:d for d in h_annotations}
#         for k in h_annotations.keys():
#             h_annotations[k]['tweet_id'] = tweet_id
            
#         delete = set(db_annotations.keys()) - set(h_annotations.keys())
#         for k in delete:
#             self.delete_annotation(db_annotations[k]['id'])
#             num_deleted += 1
            
#         add = set(h_annotations.keys()) - set(db_annotations.keys())
#         for k in add:
#             self.add_annotation(h_annotations[k])
#             num_added += 1
            
#         update = set(h_annotations.keys()) & set(db_annotations.keys())
#         for k in update:
#             if self.update_annotation(h_annotations[k], db_annotations[k]['id']):
#                 num_updated += 1
            
#         return {'annotations': num_annotations, 'updated': num_updated, 'added': num_added, 'deleted': num_deleted}


# class HtmlGenerator():
#     def __init__(self):
#         return
    
#     def get_all_records_view(self, data):
#         html = f"<a id='next-record' href=/next>Next Record</a>"
        
#         html += "<table id='all-records-table'>"
        
#         cols = [k for k,v in data[0].items()]
#         html += "<tr class='header'>"
#         for j,k in enumerate(cols):
#             html += f"<th class='col-{j}'>{k}</th>"
#         html += "</tr>"
        
        
#         for i,d in enumerate(data):
#             html += "<tr class='row-{i}''>"
#             for j,k in enumerate(cols):
#                 html += f"<td class='col-{j}'>{d[k]}</td>"
#             html += f"<td class='link'><a href=/record/{d[cols[-1]]}>link</a></td>"
#             html += "</tr>"
        
#         html += "</table>"
        
#         return f"<html><body>{html}</body></html>"
        
#     def get_record_view(self, data):
#         html = f"<a id='next-record' href=/next>Next Record</a>"
        
#         for prefix,value,indentation in self.rec_parse_lists(data):
#             p = '-'.join([str(p) for p in prefix])
#             html += f"<div class='{p}'><span class='key'>{p}</span>: <span class='value'>{value}</span></div>"
        
#         html += f"<div id='update-annotations'>\
#                       <div>\
#                           <button type='button'>Update Annotations</button>\
#                       </div>\
#                       <div id='response'></div>\
#                   </div>"
        
#         return f"<html>\
#                      <head>\
#                          <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>\
#                          <script src='/static/js/update-button.js'></script>\
#                      </head>\
#                      <body>\
#                          {html}\
#                      </body>\
#                  </html>"
        
#     def rec_parse_lists(self, l, nesting=0, prefix=[]):
#         elements = []
#         if isinstance(l, dict):
#             for k,v in l.items():
#                 elements += self.rec_parse_lists(v, nesting+1, prefix+[k])
#             return elements

#         if isinstance(l, (list,tuple,set)):
#             for i,v in enumerate(l):
#                 elements += self.rec_parse_lists(v, nesting+1, prefix+[i])
#             return elements

#         elements.append((prefix, l, nesting))
#         return elements

# class TweetHtmlGenerator(HtmlGenerator):
#     def __init__(self):
#         return
    
#     def get_all_records_view(self, data):
#         records_data = [[d['twitter_id'],d['annotation_priority'],d['annotation_count'],f"<a href='/record/{d['twitter_id']}'>link</a>'"] for d in data if d['annotation_priority'] > 0]
        
#         html = f"<div id='controls'>\
#                      <div id='next-annotation'>\
#                          <a class='link' href=/random-record>Random record</a>\
#                      </div>\
#                  </div>"
        
#         html += "<div id='content'>\
#                      <table id='records-table' class='display'>\
#                          <thead>\
#                              <tr>\
#                                  <th>Twitter ID</th>\
#                                  <th>Annotation Priority</th>\
#                                  <th># Annotations</th>\
#                                  <th>Link</th>\
#                              </tr>\
#                          </thead>\
#                          <tfoot>\
#                              <tr>\
#                                  <th>Twitter ID</th>\
#                                  <th>Annotation Priority</th>\
#                                  <th># Annotations</th>\
#                                  <th>Link</th>\
#                              </tr>\
#                          </tfoot>\
#                      </table>\
#                  </div>"
        
#         return f"<html>\
#                      <head>\
#                          <link rel='stylesheet' href='https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css'>\
#                          <link rel='stylesheet' href='/static/css/style.css'>\
#                          <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>\
#                          <script src='https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js'></script>\
#                          <script src='/static/js/records-table.js'></script>\
#                          <script>var records_data = {json.dumps(records_data)};</script>\
#                      </head>\
#                      <body>\
#                          <div id='wrapper'>\
#                              {html}\
#                          </div>\
#                      </body>\
#                  </html>"
    
#     def get_record_view(self, data):
#         html = f"<div id='controls'>\
#                      <div id='next-annotation'>\
#                          <a class='link' href=/random-record>Random record</a>\
#                      </div>\
#                      <div id='to-list'>\
#                          <a class='link' href=/list-records>Back to records list</a>\
#                      </div>\
#                      <div id='update-annotations'>\
#                          <div class='link'>Update annotations</div>\
#                          <div class='response'></div>\
#                      </div>\
#                  </div>"
        
#         html += f"<div id='content'>\
#                       <div class='metadata'>\
#                           <div><span class='key'>Twitter ID</span>: <span class='value'>{data['tweet_data']['twitter_id']}</span></div>\
#                           <div><span class='key'>URL</span>: <span class='value'>{data['tweet_data']['url']}</span></div>\
#                           <div><span class='key'>Date</span>: <span class='value'>{data['tweet_data']['date']}</span></div>\
#                           <div><span class='key'>User</span>: <span class='value'>{data['tweet_data']['user_name']}</span></div>\
#                           <div><span class='key'>Annotation priority</span>: <span class='value'>{data['tweet_data']['annotation_priority']}</span></div>\
#                       </div>\
#                       <div class='tweet-text'>{data['tweet_data']['text']}</div>\
#                   </div>"
        
#         return f"<html>\
#                      <head>\
#                          <link rel='stylesheet' href='/static/css/style.css'>\
#                          <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>\
#                          <script src='/static/js/update-button.js'></script>\
#                      </head>\
#                      <body>\
#                          <div id='wrapper'>\
#                              {html}\
#                          </div>\
#                      </body>\
#                  </html>"

# class AnnotationHTTPRequestHandler(BaseHTTPRequestHandler):
    
#     def handle_next(self):
#         return
    
#     def handle_update(self):
#         return
    
#     def handle_record(self):
#         return
    
#     def handle_list(self):
#         return
    
#     def do_GET(self):
#         self.endpoints = {
#             'record': self.handle_record,
#             'list': self.handle_list,
#             'update': self.handle_update,
#             'next': self.handle_next
#         }
        
#         self.root_dir = "."

#         self.suffix_map = {
#             'js': "application/javascript",
#             'css': "text/css",
#             'html': "text/html",
#             'htm': "text/html",
#             'xml': "text/xml",
#             'csv': "text/csv",
#             'json': "application/json",
#             'gif': "image/gif"
#         }
        
#         # check if path is a file
#         fp = f"{self.root_dir}{self.path}"
#         if os.path.isfile(fp):
#             suffix = re.findall('\.([^.]+)$',fp)
#             # figure out the content type from the filename suffix
#             content_type = "text/plain"
#             if len(suffix):
#                 suffix = suffix[0]
#                 if suffix in self.suffix_map:
#                     content_type = self.suffix_map[suffix]
            
#             if suffix in {'gif'}:
#                 with open(fp, 'rb') as f:
#                     self.send_response(200)
#                     self.send_header("Content-type", content_type)
#                     self.end_headers()

#                     self.wfile.write(f.read())  # send the file

#                     return
#             else: 
#                 with open(fp, 'rt') as f:
#                     self.send_response(200)
#                     self.send_header("Content-type", content_type)
#                     self.end_headers()

#                     self.wfile.write(f.read().encode('utf8'))  # send the file

#                     return
        
#         # if not, check if it's an endpoint
#         endpoint = ""
#         d = re.findall('^.*?/(.*?)($|/)', self.path)
#         if len(d):
#             endpoint = d[0][0]
#         else:
#             self.send_response(404)
#             self.send_header("Content-type", "text/html")
#             self.end_headers()
            
#             self.wfile.write(b'No endpoint detected <br>')
            
#             return
        
#         if endpoint in self.endpoints:
#             self.endpoints[endpoint]()
#         else:
#             self.send_response(404)
#             self.send_header("Content-type", "text/html")
#             self.end_headers()

#             self.wfile.write(f'{endpoint} is not valid endpoint <br>'.encode('utf8'))
            
#             return

# class TweetHTTPRequestHandler(AnnotationHTTPRequestHandler):
    
#     db_manager = TweetDatabaseHandler()
#     html_gen = TweetHtmlGenerator()
#     hypothesis_manager = TweetHypothesisHandler()

    
#     def handle_next(self):
#         next_record = self.db_manager.get_next_record()
#         url = f"record/{next_record['twitter_id']}"
        
#         self.send_response(302)
#         self.send_header('Location', url)
#         self.end_headers()
        
#         return
    
#     def handle_update(self):
#         referer = self.headers.get("referer")
#         d = re.findall('^.*?/([a-zA-Z0-9]+)$', referer)
#         if len(d):
#             twitter_id = d[0]
                
#             annotations = self.hypothesis_manager.get_annotations(referer)
#             r = self.db_manager.update_annotations(twitter_id, list(annotations))

#             self.send_response(200)
#             self.send_header("Content-type", "application/json")
#             self.end_headers()

#             self.wfile.write(json.dumps(r).encode('utf8'))

#             return 
        
#         self.send_response(404)
#         self.send_header("Content-type", "text/html")
#         self.end_headers()

#         self.wfile.write(f'Invalid path for "update" endpoint <br>'.encode('utf8'))
            
#         return
    
#     def handle_record(self):
#         d = re.findall('^.*?/([a-zA-Z0-9]+)$', self.path)
#         if len(d):
#             twitter_id = d[0]

#             data = self.db_manager.get_record(twitter_id)
#             html = self.html_gen.get_record_view(data)

#             self.send_response(200)
#             self.send_header("Content-type", "text/html")
#             self.end_headers()

#             self.wfile.write(html.encode('utf8'))

#             return

#         self.send_response(404)
#         self.send_header("Content-type", "text/html")
#         self.end_headers()

#         self.wfile.write(f'Invalid path for "record" endpoint <br>'.encode('utf8'))

#         return
    
#     def handle_list(self):
#         data = self.db_manager.get_all_records()
#         html = self.html_gen.get_all_records_view(data)
        
#         self.send_response(200)
#         self.send_header("Content-type", "text/html")
#         self.end_headers()
        
#         self.wfile.write(html.encode('utf8'))

#         return

