from app import app, bp, doc_store, html_gen

@bp.route('/record/<set_id>')
def single_record(set_id):
    doc = doc_store.load_document(set_id)
    return html_gen.gen_html(doc)