from flask import Flask, Blueprint
from utils import *
import ontotextquery
import os

app = Flask(__name__)

# get environmental variables
DATA_DIR = os.getenv('DATA_DIR')
# db_manager = TweetDatabaseHandler(db_path=f"{DB_TYPE}://{DB_USERNAME}:{DB_PASSWORD}@{DB_URL}:{DB_PORT}/{DB_NAME}")

doc_store = ontotextquery.splparse.DocumentStore()
doc_store.connect_db(f'{DATA_DIR}/all_spls.sqlite')
html_gen = ontotextquery.splparse.DocHtmlGenerator(fp=f'{DATA_DIR}/all_html_escape_chars.tsv')
source = DataSource(data_dir=DATA_DIR)

URL_PREFIX = os.getenv('URL_PREFIX')
bp = Blueprint('main', __name__, template_folder='templates', static_folder='static', url_prefix=URL_PREFIX)

from app import routes

app.register_blueprint(bp, url_prefix=URL_PREFIX)
