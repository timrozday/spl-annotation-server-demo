from app import app, db_manager, hypothesis_manager
from flask import render_template, request, redirect
import json

@app.route('/')
@app.route('/list-records')
def all_records():
    data = db_manager.get_all_records()
    return render_template("all-records.html")

@app.route('/list-records-data')
def all_records_data():
    data = db_manager.get_all_records()
    data = [[d['twitter_id'],d['annotation_priority'],d['annotation_count'],f"<a href='/record/{d['twitter_id']}'>link</a>'"] for d in data if d['annotation_priority'] > 0]
    return {'records_data': data}

@app.route('/record/<record_id>')
def single_record(record_id):
    data = db_manager.get_record(record_id)
    return render_template("single-record.html", data=data)
    
@app.route('/update-record')
def update_record():
    url = request.referrer
    record_id = url.split('/')[-1]
    annotations = hypothesis_manager.get_annotations(url)
    r = db_manager.update_annotations(record_id, list(annotations))
    return r
    
@app.route('/random-record')
def random_record():
    next_record = db_manager.get_next_record()
    url = f"record/{next_record['twitter_id']}"
    return redirect(url, code=302)