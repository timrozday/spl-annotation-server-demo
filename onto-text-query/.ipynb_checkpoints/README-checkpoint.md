# `textquery`

Used to query short strings and large documents for terms found in a collection of ontologies. 

Identifies ontology terms in text, groups them by ontology relationships and prioritises them based on various rules.

Intended application is assignment of terms identified by NLP models to ontology entities, and for picking out any ontology entities in a chunk of text.

Relies on database of parsed ontologies where terms and relationships are extracted, stored and indexed.

Text matching can be "fuzzy" by allowing "expansion" query text and ontology terms and allowing some gaps in the match-path. The fuzzyness operates on a token level rather than a character level (it does not do anything similar to Levenshtein distance).