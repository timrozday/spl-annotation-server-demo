import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="onto-text-query",
    version="0.0.1",
    author="Tim Rozday",
    author_email="timrozday@ebi.ac.uk",
    description="Querying short strings and large documents for terms found in a collection of ontologies",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/timrozday/onto-text-query",
    packages=setuptools.find_namespace_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)