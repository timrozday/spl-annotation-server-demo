import re
import itertools as it
from html import unescape
import copy
from lxml import etree
import sqlite3
import sqlalchemy
import requests
import json

from tqdm.auto import tqdm

import spacy
import scispacy
# from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES
# from spacy.lang.char_classes import ALPHA, ALPHA_LOWER, ALPHA_UPPER, CONCAT_QUOTES, LIST_ELLIPSES, LIST_ICONS
# from spacy.util import compile_prefix_regex, compile_infix_regex, compile_suffix_regex

import pdb

class SentenceParser():
    # Creates connected sentences
    # Handles sentence exapansion
    def __init__(self, tokenizer, expanders=[]):
        self.tokenizer = tokenizer
        self.expanders = expanders
        self.stop_words = {'of', 'type', 'with', 'and', 'the', 'or', 'due', 'in', 
                           'to', 'by', 'as', 'a', 'an', 'is', 'for', '.', ',', 
                           ':', ';', '?', '-', '(', ')', '/', '\\', '\'', '"', 
                           '\n', '\t', '\r'}
    
    def parse(self,string,split_sentence=True):
        conn_sentences, sentence_locs = self.tokenizer.tokenize(string, split_sentence=split_sentence)
        return conn_sentences, sentence_locs
                   
    def expand(self, conn_sentence):
        for e in self.expanders:
            conn_sentence = e.expand(conn_sentence)
        return conn_sentence
    
    def register_expanders(self, expanders):
        if not isinstance(expanders,list):
            expanders = [expanders]
        self.expanders += expanders
    
    def _rec_gen_kmers(self, kmer, n):
        if len(kmer) >= n: 
            return {tuple(kmer)}
        if not kmer[-1] in self.conn_sentence.conn.keys(): 
            return set()
        results = set()
        for token_i in self.conn_sentence.next_skip_stop_words(kmer[-1], self.stop_words):
            if token_i is None: continue
            results.update(self._rec_gen_kmers(kmer+[token_i], n))
        return results

    def gen_kmers(self, n, conn_sentence=None):
        if not conn_sentence is None:
            self.conn_sentence = conn_sentence
        
        kmers = set()
        for token_i,token in conn_sentence.tokens.items():
            if token.string.lower() in self.stop_words: continue
            kmers.update(self._rec_gen_kmers([token_i], n))
        return kmers
        
        

    
        
class KmerIndex():
    # provides interface with the query index
    # handles building of the index from a list of sentences
    def __init__(self,path):
        self.path = path
        
    def add_term(self,connected_sentence,identifier):
        return
        
    def remove_term(self,connected_sentence,identifier):
        return
    
    def add_terms(self,terms):
        # can I speed things up by processing them in a batch?
        for connected_sentence, identifier in terms:
            self.add_term(connected_sentence,identifier)
    
    def remove_terms(self,terms):
        # can I speed things up by processing them in a batch?
        for connected_sentence, identifier in terms:
            self.remove_term(connected_sentence,identifier)
    
    def query(self,query):
        return
        
    def connect_db(self):
        sqlite3.connect(self.path)
    
    def get_db(self):
        return
    
    def close(self):
        return
    
    
    
class ConnectedSentenceQuery():
    def __init__(self, tokenizer, kmer_index, lemmatizer=None):
        self.tokenizer = tokenizer
        self.kmer_index = kmer_index
        self.lemmatizer = lemmatizer
    
    def connect_index(self):
        self.kmer_index.connect_db()
    
    def close_index(self):
        self.kmer_index.close()
        
    def process_query(self,query):
        return
        
    def kmer_query(self,query,connected_sentence):
        return
    
    def all_words_check(self,potential_match,connected_sentence):
        return
    
    def connected_query(self,potential_match,connected_sentence):
        return
    
    def query(self,query,connected_sentence):
        matches = []
        potential_matches = self.kmer_query(query,connected_sentence)
        for potential_match in results:
            if self.all_words_check(potential_match,connected_sentence):
                match = self.connected_query(potential_match,connected_sentence)
                if match: 
                    matches.append(match)
        
        return matches
    










# def rec_gen_sentences(conn, sentence=[None]):
#     #if not sentence[-1] in conn: return {tuple(sentence)}
#     sentences = set()
#     for next_id in conn[sentence[-1]]:
#         if next_id is None: 
#             sentences.add(tuple(sentence[1:]))
#             continue
#         sentences.update(rec_gen_sentences(conn, sentence=sentence+[next_id]))
#     return sentences


class ConnectedSentenceExpander():
    def __init__(self):
        return
    def expand(self,conn_sentence):
        return conn_sentence
                   
class LemmaExpander(ConnectedSentenceExpander):
    def __init__(self,lemmatizer):
        self.lemmatizer = lemmatizer
                   
    def expand(self,conn_sentence):
        tokens = list(conn_sentence.tokens.items())
        for token_i,token in tokens:
            lemma = self.lemmatizer.lemmatize(token.string, token.pos)
            if not lemma.lower() == token.string.lower(): 
#                 # add alternate token
#                 new_token = copy.deepcopy(token)
#                 new_token.string = lemma
#                 new_token_i = conn_sentence.add_token(new_token)
#                 conn_sentence.copy_conn(token_i,new_token_i)
                conn_sentence.tokens[token_i].string = lemma  # replace string of token
        return conn_sentence
                   
class BracketExpander(ConnectedSentenceExpander):
    def expand(self,conn_sentence):
         # find brackets     
        open_brackets = {i for i,t in conn_sentence.tokens.items() if t.string == '('}
        close_brackets = {i for i,t in conn_sentence.tokens.items() if t.string == ')'}
        used_brackets = set()
        brackets = set()
        for open_bracket in sorted(open_brackets, reverse=True):
            try: close_bracket = sorted({i for i in (close_brackets - used_brackets) if i > open_bracket})[0]
            except: continue
            brackets.add((open_bracket, close_bracket))
            used_brackets.add(close_bracket)

        # add conns based on brackets
        # skip brackets
        for bracket_id in (open_brackets | close_brackets):
            previous_ids = conn_sentence.rev_conn[bracket_id]
            next_ids = conn_sentence.conn[bracket_id]
            for id1,id2 in it.product(previous_ids, next_ids):
                conn_sentence.add_conn(id1,id2)

        # skip bracket contents
        for open_bracket, close_bracket in brackets:
            previous_ids = conn_sentence.rev_conn[open_bracket]
            next_ids = conn_sentence.conn[close_bracket]
            for id1,id2 in it.product(previous_ids, next_ids):
                conn_sentence.add_conn(id1,id2)

        return conn_sentence

class HyphenSlashExpander(ConnectedSentenceExpander):
    def expand(self,conn_sentence):
        # find all hyphenated/slash words
        for i,token in conn_sentence.tokens.items():
            if token.string in {'-', '/'}:
                # get previous and next words
                if i in conn_sentence.rev_conn:
                    previous_ids = conn_sentence.rev_conn[i]
                else: 
                    previous_ids = set()
                if i in conn_sentence.conn:
                    next_ids = conn_sentence.conn[i]
                else: 
                    next_ids = set()

                # go one more step forward and back
                prev_prev_ids = set()
                for t_i in previous_ids:
                    if t_i in conn_sentence.rev_conn:
                        prev_prev_ids.update(conn_sentence.rev_conn[t_i])
                next_next_ids = set()
                for t_i in next_ids:
                    if t_i in conn_sentence.conn:
                        next_next_ids.update(conn_sentence.conn[t_i])

                #  prev --> next
                for p in previous_ids:
                    for n in next_ids:
                        conn_sentence.add_conn(p,n)
                #  prev_prev --> next      
                for pp in prev_prev_ids:
                    for n in next_ids:
                        conn_sentence.add_conn(pp,n)
                #  prev --> next_next
                for p in previous_ids:
                    for nn in next_next_ids:
                        conn_sentence.add_conn(p,nn)

        return conn_sentence

class ListExpander(ConnectedSentenceExpander):
    def _rec_gen_sentences(self, conn_sentence, sentence=[None]):
        sentences = set()
                   
        if not sentence[-1] in conn_sentence.conn:
            raise Exception(f"{sentence[-1]} not in ConnectedSentence connections, this shouldn't happen")
                   
        for next_id in conn_sentence.conn[sentence[-1]]:
            if next_id is None: 
                sentences.add(tuple(sentence[1:]))
                continue
            sentences.update(self._rec_gen_sentences(conn_sentence, sentence=sentence+[next_id]))
        return sentences
                   
    def expand(self,conn_sentence):
        sentences_ids = self._rec_gen_sentences(conn_sentence)

        # detect lists in each of these sentences, and make the nessesary adjustments
        for sentence_ids in sentences_ids:
            tags = [f"{i}{conn_sentence.tokens[i].tag}" for i in sentence_ids]
            s_pos_map = {token_id:i for i,token_id in enumerate(sentence_ids)}
            text_lists = re.findall('((?:(?:\d+(?:JJ|DT|NN|NNS|\?))+\d+(?:,|CC))+(?:\d+(?:JJ|DT|NN|NNS|\?))+)', "".join(tags)) # find all the sentences
            # generate all the versions of each list
            all_combs = []
            for text_list in text_lists:
                head_and_tails_ids = set()

                heads = re.findall('^((?:\d+(?:JJ|DT|NN|NNS|CC|\?))+)(?:\d+,|$)', text_list)
                for i,h in enumerate(heads):
                    hs = re.findall('(?:(\d+)(?:JJ|DT|NN|NNS|\?))', h)
                    heads[i] = [int(a) for a in hs]
                    head_and_tails_ids.update(heads[i])

                tails = re.findall('(?:\d+,|^)((?:\d+(?:JJ|DT|NN|NNS|CC|\?))+)$', text_list)
                if len(tails) > 0:
                    tails = tails[0]
                    tails = re.findall('((?:\d+(?:JJ|DT|NN|NNS|\?))+)', tails)
                    for i,t in enumerate(tails):
                        ts = re.findall('(?:(\d+)(?:JJ|DT|NN|NNS|\?))', t)
                        tails[i] = [int(a) for a in ts]
                        head_and_tails_ids.update(tails[i])
                else: tails = []


                middle = re.findall('((?:\d+(?:JJ|DT|NN|NNS|\?))+)', text_list)
                middles = []
                for i,m in enumerate(middle):
                    ms = re.findall('(?:(\d+)(?:JJ|DT|NN|NNS|\?))', m)
                    ms = [int(a) for a in ms]
                    ms = [a for a in ms if not a in head_and_tails_ids]
                    if len(ms)>0: middles.append(ms)

                # make new connections
                for m in middles:
                    m = [int(i) for i in m]
                    # add begining to all heads
                    for h in heads:
                        for h_id in h:
                            if s_pos_map[h_id] < s_pos_map[m[0]]:
                                conn_sentence.add_conn(h_id,m[0])
                    # add end to all tails
                    for t in tails:
                        for t_id in t:
                            if s_pos_map[m[-1]] < s_pos_map[t_id]:
                                conn_sentence.add_conn(m[-1],t_id)

                for h in heads:
                    for h_id in h:
                        for t in tails:
                            for t_id in t:
                                if s_pos_map[h_id] < s_pos_map[t_id]:
                                    conn_sentence.add_conn(h_id,t_id)
                   
        return conn_sentence

    
    
    
    
    
    
    
    
    
    
def expand_thesaurus(sentence, matches, query_f, f_args, stop_words={'of', 'type', 'with', 'and', 'the', 'or', 'due', 'in', 'to', 'by', 'as', 'a', 'an', 'is', 'for', '.', ',', ':', ';', '?', '-', '(', ')', '/', '\\', '\'', '"', '\n', '\t', '\r'}):
    # organise thesaurus matches by location, just store the match code (not the string/sentence)
    match_locs = {}
    for match in matches:
        for p in match['paths']:
            try: match_locs[p].add(f"{match['source']};{match['onto_id']}")
            except: match_locs[p] = {f"{match['source']};{match['onto_id']}"}
    
    new_id = max([w['id'] for k,w in sentence['words'].items()]) + 1
    for path_ids, matches in match_locs.items():
        # add codes to sentence
        new_ids = set()
        for code in matches:
            sentence['words'][new_id] = {'id': new_id, 'word': code, 'parent': path_ids}
            new_ids.add(new_id)
            new_id+=1
        
        # add connections to the codes
        for code_id in new_ids:
            sentence['rev_conn'] = gen_rev_conn(sentence['conn'])
            for s_id in sentence['rev_conn'][path_ids[0]]:
                sentence['conn'][s_id].add(code_id)
            for s_id in sentence['conn'][path_ids[-1]]:
                try: sentence['conn'][code_id].add(s_id)
                except: sentence['conn'][code_id] = {s_id}
        sentence['rev_conn'] = gen_rev_conn(sentence['conn'])
        
    return sentence

def expand_index(sentence, kmer_query_f, names_query_f, kmer_query_args, names_query_args):
    # find matches from the thesaurus indexes
    matches = kmer_query(sentence, kmer_query_f, kmer_query_args, stop_words=set())
    matches = all_word_query(sentence, matches, stop_words=set())
    matches = loc_query(sentence, matches, stop_words=set())
    
    # substitute names from the matches into the sentence
    expanded_sentence = expand_thesaurus(sentence, matches, names_query_f, names_query_args)
    
    return expanded_sentence

def expand_sentence(original_sentence):
    # add other conns based on parentheses, hyphens, slashes and lists
    sentence = expand_lists(original_sentence)
    sentence = expand_brackets(sentence)
    sentence = expand_hyphen_slash(sentence)

    return sentence

               
                   
def generate_kmers(l,n):
    for i in range(len(l)-n+1):
        yield tuple(sorted(l[i:i+n]))

def generate_ordered_kmers(l,n):
    for i in range(len(l)-n+1):
        yield l[i:i+n]

def query_index_db(kmer, db_conn):
    kmer_str = re.sub('\'', '\'\'', repr(kmer)) # handle quotes
    results = db_conn.execute(f"select st.onto_id, st.predicate_type, st.source, st.string, st.expanded_sentences from (select * from (select * from kmers where kmer='{kmer_str}') km left join kmer_to_string on km.id=kmer_to_string.kmer_id) ks inner join (select * from strings) st on ks.string_id=st.id")
    for onto_id, predicate_type, source, string, sentence in results:
        yield {'source': source, 'onto_id': onto_id, 'predicate_type': predicate_type, 'string': string, 'sentence': eval(sentence)}

def kmer_query(sentence, query_f, f_args, stop_words={'of', 'type', 'with', 'and', 'the', 'or', 'due', 'in', 'to', 'by', 'as', 'a', 'an', 'is', 'for', '.', ',', ':', ';', '?', '-', '(', ')', '/', '\\', '\'', '"', '\n', '\t', '\r'}):
    # generate kmers from conn
    conn = sentence['conn']
    word_index = sentence['words']
    kmers = {}
    for n in range(1,4):
        kmers[n] = set()
        kmer_ids = conn_gen_kmers(sentence, n, stop_words=stop_words)
        for kmer_id in kmer_ids:
            kmer = tuple(sorted([word_index[k]['word'].lower() for k in kmer_id]))
            kmers[n].add(kmer)

    # get matches
    for n in range(1,4):
        for kmer in kmers[n]:
            # query the database here with the kmer
            matches = query_f(kmer, *f_args)
            for m in matches: yield m

# don't use!
def all_word_query(sentence, matches, stop_words={'of', 'type', 'with', 'and', 'the', 'or', 'due', 'in', 'to', 'by', 'as', 'a', 'an', 'is', 'for', '.', ',', ':', ';', '?', '-', '(', ')', '/', '\\', '\'', '"', '\n', '\t', '\r'}):
    sentence_words = {v['word'].lower() for k,v in sentence['words'].items()}
    for match in matches:
        for match_word_ids in rec_gen_sentences(match['sentence']['conn']):
            match_words = {match['sentence']['words'][i]['word'].lower() for i in match_word_ids}
            match_words = match_words - stop_words
            if len(match_words - sentence_words) == 0: 
                yield match
                break

def loc_query(sentence, matches, stop_words={'of', 'type', 'with', 'and', 'the', 'or', 'due', 'in', 'to', 'by', 'as', 'a', 'an', 'is', 'for', '.', ',', ':', ';', '?', '-', '(', ')', '/', '\\', '\'', '"', '\n', '\t', '\r'}):
    sentence['rev_conn'] = gen_rev_conn(sentence['conn'])
    for match in matches:
        match['sentence']['rev_conn'] = gen_rev_conn(match['sentence']['conn'])

        # get all matching paths
        # find all start points for match paths
        match_start_ids = next_conn_skip_stop_words(match['sentence'], None, stop_words)
        match_start_words = {}
        for word_id in match_start_ids:
            if word_id is None: continue
            word = match['sentence']['words'][word_id]['word'].lower()
            try: match_start_words[word].add(word_id)
            except: match_start_words[word] = {word_id}

        sentence_start_words = {}
        for match_start_word in match_start_words.keys():
            sentence_start_words[match_start_word] = {k for k,v in sentence['words'].items() if v['word'].lower() == match_start_word}

        start_ids = set()
        for word in match_start_words.keys():
            start_ids.update(it.product(sentence_start_words[word], match_start_words[word]))
        
        # for each of the start points, get the paths
        paths = set()
        for sentence_start_id, match_start_id in start_ids:
            #r = rec_conn_get_common_paths(sentence, match['sentence'], [(sentence_start_id,0)], [(match_start_id,0)], stop_words)
            r = conn_get_common_paths(sentence, match['sentence'], sentence_start_id, match_start_id, stop_words)
            #paths.update({p[0] for p in r})
            paths.update({tuple(p['sentence_path']) for p in r})

        good_paths = set()
        for path in paths:
            path_ids = tuple([p[0] for p in path])

            # filter out paths that have too many gaps
            gap_sum = sum([p[1] for p in path])
            if gap_sum > len(path_ids)/3: continue

            # filter out paths that don't make it to the end of the match
            #if len(path_ids & match['sentence']['rev_conn'][None]) == 0: continue
            
            good_paths.add(path_ids)

        if len(good_paths)>0: 
            match['paths'] = good_paths.copy()
            yield match

def rec_conn_get_common_paths(sentence, match_sentence, sentence_path, match_path, stop_words):
    paths = set()

    sentence_next_ids = next_conn_skip_stop_words(sentence, sentence_path[-1][0], stop_words)
    match_next_ids = next_conn_skip_stop_words(match_sentence, match_path[-1][0], stop_words)

    # allow for gaps
    sentence_next_next_ids = set()
    for sentence_next_id in sentence_next_ids:
        if sentence_next_id is None: continue
        sentence_next_next_ids.update(next_conn_skip_stop_words(sentence, sentence_next_id, stop_words))

    # form dictionary where each word has locs and the number of gaps needed to reach that loc
    sentence_next_words = {}
    for word_id in sentence_next_ids:
        if word_id is None: continue
        word = sentence['words'][word_id]['word'].lower()
        try: sentence_next_words[word].add((word_id,0))
        except: sentence_next_words[word] = {(word_id,0)}

    for word_id in sentence_next_next_ids:
        if word_id is None: continue
        word = sentence['words'][word_id]['word'].lower()
        try: sentence_next_words[word].add((word_id,1))
        except: sentence_next_words[word] = {(word_id,1)}

    match_next_words = {}
    for word_id in match_next_ids:
        if word_id is None:
            paths.add((tuple(sentence_path), tuple(match_path)))
            continue
        word = match_sentence['words'][word_id]['word'].lower()
        try: match_next_words[word].add((word_id,0))
        except: match_next_words[word] = {(word_id,0)}

    # generate list of words that both sentence and match can move to, and generate all the word_ids that fascilitate this transition to the next words
    next_words = set(match_next_words.keys()) & set(sentence_next_words.keys())
    next_ids = set()
    for word in next_words:
        next_ids.update(it.product(sentence_next_words[word], match_next_words[word]))

    for sentence_next_id, match_next_id in next_ids:
        paths.update(rec_conn_get_common_paths(sentence, match_sentence, sentence_path+[sentence_next_id], match_path+[match_next_id], stop_words))

    return paths

def conn_get_common_paths(sentence, match_sentence, sentence_start_id, match_start_id, stop_words):
    matched_paths = [{'sentence_path': [(sentence_start_id,0)], 'match_path': [(match_start_id,0)]},]
    confirmed_matched_paths = []
    while True:
        new_matched_paths = []
        for matched_path in matched_paths:
            sentence_next_ids = next_conn_skip_stop_words(sentence, matched_path['sentence_path'][-1][0], stop_words)
            match_next_ids = next_conn_skip_stop_words(match_sentence, matched_path['match_path'][-1][0], stop_words)

            # allow for gaps
            sentence_next_next_ids = set()
            for sentence_next_id in sentence_next_ids:
                if sentence_next_id is None: continue
                sentence_next_next_ids.update(next_conn_skip_stop_words(sentence, sentence_next_id, stop_words))

            # form dictionary where each word has locs and the number of gaps needed to reach that loc
            sentence_next_words = {}
            for word_id in sentence_next_ids:
                if word_id is None: continue
                word = sentence['words'][word_id]['word'].lower()
                try: sentence_next_words[word].add((word_id,0))
                except: sentence_next_words[word] = {(word_id,0)}

            for word_id in sentence_next_next_ids:
                if word_id is None: continue
                word = sentence['words'][word_id]['word'].lower()
                try: sentence_next_words[word].add((word_id,1))
                except: sentence_next_words[word] = {(word_id,1)}

            match_next_words = {}
            for word_id in match_next_ids:
                if word_id is None:
                    confirmed_matched_paths.append(matched_path)
                    continue
                word = match_sentence['words'][word_id]['word'].lower()
                try: match_next_words[word].add((word_id,0))
                except: match_next_words[word] = {(word_id,0)}

            # generate list of words that both sentence and match can move to, and generate all the word_ids that fascilitate this transition to the next words
            next_words = set(match_next_words.keys()) & set(sentence_next_words.keys())
            next_ids = set()
            for word in next_words:
                next_ids.update(it.product(sentence_next_words[word], match_next_words[word]))

            for sentence_next_id, match_next_id in next_ids:
                new_matched_paths.append({'sentence_path': matched_path['sentence_path']+[sentence_next_id], 
                                          'match_path': matched_path['match_path']+[match_next_id]})
        
        if len(new_matched_paths)>0: matched_paths = new_matched_paths
        else: break
        
    return confirmed_matched_paths

def query_sentence(sentence, db_conn):
    # do querying
    matches = kmer_query(sentence, query_index_db, [db_conn])            
    #matches = all_word_query(sentence, matches)
    matches = loc_query(sentence, matches)

    return list(matches)

def rec_query(node, loc, db_conn):
    results = {}
    if node['tag'] == "string":
        for i,sentence in enumerate(node['nodes']):
            new_loc = loc+[i]
            expanded_sentence = expand_sentence(sentence)
            matches = query_sentence(expanded_sentence, db_conn)

            if len(matches)>0:
                results[tuple(new_loc)] = {'loc': tuple(new_loc), 'sentence': sentence, 'matches': matches}
            
    else:
        for i,n in enumerate(node['nodes']):
            new_loc = loc+[i]
            rec_results = rec_query(n, new_loc, db_conn)
            for k,v in rec_results.items():
                results[k] = v
    
    return results

def onto_index_db_query(sections, db_conn):
    all_results = []
    for i, section in enumerate(sections):
        if not section is None: 
            results = rec_query(section, [i], db_conn)
            all_results.append(results)
    return all_results

def query_thesauruses(kmer, db_conn, sources):
    kmer_str = re.sub('\'', '\'\'', repr(kmer)) # handle quotes
    results = db_conn.execute(f"select st.onto_id, st.source, st.string, st.expanded_sentences from (select * from (select * from kmers where kmer='{kmer_str}') km left join kmer_to_string on km.id=kmer_to_string.kmer_id) ks inner join (select * from strings) st on ks.string_id=st.id")
    for onto_id, source, string, sentence in results:
        if source in sources: yield {'source': source, 'onto_id': onto_id, 'string': string, 'sentence': eval(sentence)}

def query_names_indexes(onto_id, db_conn):
    results = db_conn.execute(f"select source, string, predicate_type, sentences, expanded_sentences from strings where onto_id='{onto_id}'")
    for source, string, predicate_type, sentences, expanded_sentences in results:
        yield {'string': string, 'sentence': eval(expanded_sentences), 'original_sentence': eval(sentences)}
