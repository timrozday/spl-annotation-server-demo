import re
import itertools as it
from html import unescape
import copy
from lxml import etree
import sqlite3
import sqlalchemy
import requests
import json

from tqdm.auto import tqdm

import spacy
import scispacy
# from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES
# from spacy.lang.char_classes import ALPHA, ALPHA_LOWER, ALPHA_UPPER, CONCAT_QUOTES, LIST_ELLIPSES, LIST_ICONS
# from spacy.util import compile_prefix_regex, compile_infix_regex, compile_suffix_regex

import pdb

class Document():
    def __init__(self, set_id, nodes, hier, strings, string_parents, sentences={}, rev_hier=None, rev_string_parents=None, inline_tags={'content', 'sup', 'sub', 'linkHtml', 'caption', 'string', 'sentence'}):
        self.set_id = set_id
        self.hier = hier
        self.nodes = nodes
        self.strings = strings
        self.string_parents = string_parents
        self.sentences = sentences
        
        if rev_hier: 
            self.rev_hier = rev_hier
        else:
            self.rev_hier = self._rev_hier(hier)
            
        if rev_string_parents: 
            self.rev_string_parents = rev_string_parents
        else:
            self.rev_string_parents = self._rev_hier(string_parents)
        
        self.inline_tags = inline_tags
        
    def _rev_hier(self,d):
        r = {}
        for k,vs in d.items():
            for v in vs:
                r[v] = k
        return r

    def copy(self):
        return Document(set_id=str(self.set_id),
                        nodes=copy.deepcopy(self.nodes),
                        hier=copy.deepcopy(self.hier),
                        strings=copy.deepcopy(self.strings),
                        string_parents=copy.deepcopy(self.string_parents),
                        sentences=copy.deepcopy(self.sentences),
                        rev_hier=copy.deepcopy(self.rev_hier),
                        rev_string_parents=copy.deepcopy(self.rev_string_parents))
    
    def next_node_id(self):
        if len(self.nodes) == 0: 
            return 0
        else:
            return max(self.nodes.keys())+1
    
    def next_string_id(self):
        if len(self.strings) == 0: 
            return 0
        else:
            return max(self.strings.keys())+1
    
    def get_id(self,node):
        if isinstance(node,int):
            return node
        else:
            return node.id
    def get_string_id(self,s):
        if isinstance(s,int):
            return s
        else:
            return s.id
    
    def get_all_nodes_ordered(self, roots=None):
        if roots is None:
            roots = self.get_roots()
        all_nodes = {}
        for root in roots:
            self._rec_get_all_nodes_ordered(root, 0, all_nodes)
        
        r = []
        for k,vs in sorted(all_nodes.items(), key=lambda x:x[0]):
            r += vs
        
        return r
        
    def _rec_get_all_nodes_ordered(self, node, n, all_nodes):
        if n in all_nodes: all_nodes[n].append(self.get_id(node))
        else: all_nodes[n] = [self.get_id(node)]
        
        for child in self.get_children(node):
            self._rec_get_all_nodes_ordered(child, n+1, all_nodes)
    
    def get_parent(self, node):
        node_id = self.get_id(node)
        if node_id in self.rev_hier:
            return self.rev_hier[node_id]
        else:
            return None
        
    def get_children(self, node):
        node_id = self.get_id(node)
        if node_id in self.hier:
            return self.hier[node_id]
        else:
            return []
    
    def get_siblings(self, node):
        node_id = self.get_id(node)
        parent = self.get_parent(node_id)
        if parent is None:
            return []
        else:
            return self.get_children(parent)
    
    def get_sibling_pos(self, node):
        node_id = self.get_id(node)
        siblings = self.get_siblings(node_id)
        return siblings.index(node_id)
    
    def get_node_strings(self, node, recursive=False):
        node_id = self.get_id(node)
        strings = []
        if node_id in self.string_parents:
            strings += self.string_parents[node_id]
        if recursive:
            for child_id in self.get_children(node_id):
                strings += self.get_node_strings(child_id, recursive=recursive)
        return strings
    
    def get_string_parent(self, string):
        string_id = self.get_string_id(string)
        if string_id in self.rev_string_parents:
            return self.rev_string_parents[string_id]
        else:
            return None
    
    def get_string_node_mappings(self, node, inline_tags=None):
        if inline_tags is None:
            inline_tags = self.inline_tags
        node_id = self.get_id(node)
        
        text = ""
        string_mapping = {}
        node_mapping = {}

        # check for non-inline child nodes, if there are non-inline nodes then it's not a sentence
        child_tags = {self.nodes[c_id].get_tag() for c_id in self.get_children(node_id)}
        if len(child_tags-inline_tags): 
            return text, node_mapping, string_mapping

        inline_nodes = self.get_children_of_tag(node_id, inline_tags, recursive=True)

        # stitch together child strings of node (remembering which locations correspond to which sentences and nodes)
        for child_id in sorted(inline_nodes, key=lambda x:self.nodes[x].loc):
            if child_id in self.string_parents:
                old_text = str(text)
                for s_id in self.string_parents[child_id]:
                    s = self.strings[s_id].string
                    string_mapping[s_id] = (len(text),len(text)+len(s))     
                    if len(s)>0: text += s + ' '
                node_mapping[child_id] = (len(old_text),len(text)-1)
            else:
                node_mapping[child_id] = (len(text)-1,len(text)-1)

        if len(text)>0: text = text[:-1]  # remove trailing space
        node_mapping[node_id] = (0,len(text))

        # make parent nodes inherit child node mappings
        for n_id in reversed(self.get_all_nodes_ordered(roots=[node_id])):
            if not n_id in node_mapping: continue
            b,e = node_mapping[n_id]
            
            child_locs = set()
            for c in self.get_children(n_id):
                child_locs.add(node_mapping[c])
            if len(child_locs):
                child_beginning = min([b for b,e in child_locs])
                child_end = max([e for b,e in child_locs])

                node_mapping[n_id] = (child_beginning, child_end)
        
        return text, node_mapping, string_mapping
    
    def get_children_of_tag(self, node_id, target_tags, recursive=False):
        s_nodes = []
        for child_id in self.get_children(node_id):
            tag = self.nodes[child_id].get_tag()
            if tag in target_tags: 
                s_nodes.append(child_id)
            if recursive: 
                s_nodes += self.get_children_of_tag(child_id, target_tags, recursive=recursive)
        return s_nodes
    
    def get_parents_of_tag(self, node_id, target_tags, recursive=False):
        s_nodes = []
        parent_id = self.get_parent(node_id)
        if parent_id:
            tag = self.nodes[parent_id].get_tag()
            if tag in target_tags: 
                s_nodes.append(parent_id)
            if recursive: 
                s_nodes += self.get_parents_of_tag(parent_id, target_tags, recursive=recursive)
        return s_nodes
    
    def get_roots(self):
        return set(self.hier.keys()) - set(self.rev_hier.keys())
    
    def get_leaves(self):
        return set(self.rev_hier.keys()) - set(self.hier.keys())
    
    def print_doc(self, indent='    ', L=120):
        print_string = ""
        for root in self.get_roots():
            print_string += self.rec_print_hier(root, '', indent=indent, L=L)
        return print_string
    
    def rec_print_hier(self, n_id, full_indent, indent='    ', L=120):
        tag = self.nodes[n_id].tag
        
        strings = [self.strings[s] for s in self.get_node_strings(n_id)]
        if len(strings):
            s = " ".join([f"{{{string.id}}} {string.string}" for string in strings])
            if len(s)>L: 
                s = s[:L-3-1]+'...'
        else:
            s = ''

        print_string = f'{full_indent}{n_id} [{tag}] {s}\n'

        for child_id in self.get_children(n_id):
            print_string += self.rec_print_hier(child_id, full_indent+indent, indent=indent, L=L)
            
        return print_string
    
    def print_strings(self, inline_tags=None):
        if inline_tags is None:
            inline_tags = self.inline_tags
        print_string = ""
        for root in self.get_roots():
            print_string += self.rec_print_strings(root, inline_tags=inline_tags)
        
        print_string = re.sub('(\n)+','\n\n',print_string)
        print_string = re.sub(' +',' ', print_string)
        print_string = re.sub('^\s+','', print_string)
        print_string = re.sub('\s+$','', print_string)
        print_string = re.sub('\n ','\n', print_string)
        
        return print_string
        
    def rec_print_strings(self, n_id, inline_tags=None):
        if inline_tags is None:
            inline_tags = self.inline_tags
        tag = self.nodes[n_id].get_tag()
        if tag in inline_tags:
            s = ' '
        else:
            s = '\n'
        
        s += " ".join([self.strings[s].string for s in self.get_node_strings(n_id)])

        for child_id in self.get_children(n_id):
            s += self.rec_print_strings(child_id, inline_tags=inline_tags)
            
        return s
    
    def print_sentences(self):
        doc_text = ""
        for node_id, node in sorted(self.nodes.items(), key=lambda x:x[1].loc):
            if node.tag == "sentence":
                text, node_mapping, string_mapping = self.get_string_node_mappings(node)
                doc_text += text + ' '
        return doc_text
    
    def add_conn(self, source, target, insert_before=None, insert_after=None, insert_first=False, insert_last=True, insert_at=None):
        source_id = self.get_id(source)
        target_id = self.get_id(target)
        
        self.rev_hier[target_id] = source_id
        
        if not source_id in self.hier:
            self.hier[source_id] = [target_id]
            return
        
        if not insert_at is None:
            self.hier[source_id].insert(insert_at,target_id)
            return
        if not insert_before is None:
            i = self.hier[source_id].index(self.get_id(insert_before))
            self.hier[source_id].insert(i,target_id)
            return
        if not insert_after is None:
            i = self.hier[source_id].index(self.get_id(insert_after))
            self.hier[source_id].insert(i+1,target_id)
            return
        if insert_first:
            self.hier[source_id].insert(0,target_id)
            return
        if insert_last: 
            self.hier[source_id].append(target_id)
            return
    
    def remove_conn(self, source, target):
        source_id = self.get_id(source)
        target_id = self.get_id(target)
        if target_id in self.hier[source_id]:
            self.hier[source_id].remove(target_id)
            if len(self.hier[source_id]) == 0:
                del self.hier[source_id]
        if target_id in self.rev_hier:
            del self.rev_hier[target_id]
        
    def remove_conn_parent(self, node):
        node_id = self.get_id(node)
        parent_id = self.get_parent(node_id)
        if not parent_id is None:
            self.remove_conn(parent_id, node_id)
    
    def remove_conn_children(self, node):
        node_id = self.get_id(node)
        child_ids = self.get_children(node_id)
        for child_id in child_ids:
            del self.rev_hier[child_id]
        if len(child_ids):
            del self.hier[node_id]
    
    def disconnect_node(self, node):
        self.remove_conn_parent(node)
        self.remove_conn_children(node)
    
    def add_node(self, node, parent=None, children=[], strings=[], insert_before=None, insert_after=None, insert_first=False, insert_last=True, insert_at=None):
        node_id = self.next_node_id()
        node.id = node_id
        self.nodes[node_id] = node.copy()
        
        if not parent is None:
            parent_id = self.get_id(parent)
            self.add_conn(parent_id, node_id, insert_before=insert_before, insert_after=insert_after, insert_first=insert_first, insert_last=insert_last, insert_at=insert_at)  # set parent
        self.add_children(node, children)
        self.add_strings(node, strings)
            
        return node_id
    
    def add_children(self, node, children):
        node_id = self.get_id(node)
        child_ids = [self.get_id(c) for c in children]
        for child_id in child_ids:
            self.remove_conn_parent(child_id)
            self.add_conn(node_id, child_id)
    
    def add_strings(self, node, strings):
        node_id = self.get_id(node)
        string_ids = [self.get_id(s) for s in strings]
        for string_id in string_ids:
            self.remove_string_conn(string_id)
            self.add_string_conn(node_id, string_id)
    
    def add_string_conn(self, string, parent, insert_before=None, insert_after=None, insert_first=False, insert_last=True, insert_at=None):
        string_id = self.get_string_id(string)
        parent_id = self.get_id(parent)
        
        self.rev_string_parents[string_id] = parent_id
        
        if not parent_id in self.string_parents:
            self.string_parents[parent_id] = [string_id]
            return
        
        if not insert_at is None:
            self.string_parents[parent_id].insert(insert_at,string_id)
            return
        if not insert_before is None:
            i = self.string_parents[parent_id].index(self.get_string_id(insert_before))
            self.string_parents[parent_id].insert(i,string_id)
            return
        if not insert_after is None:
            i = self.string_parents[parent_id].index(self.get_string_id(insert_after))
            self.string_parents[parent_id].insert(i+1,string_id)
            return
        if insert_first:
            self.string_parents[parent_id].insert(0,string_id)
            return
        if insert_last: 
            self.string_parents[parent_id].append(string_id)
            return
    
    def add_string(self, string, parent, insert_before=None, insert_after=None, insert_first=False, insert_last=True, insert_at=None):
        string_id = self.next_string_id()
        string.id = string_id
        self.strings[string_id] = string.copy()
        self.add_string_conn(string_id, parent, insert_before=insert_before, insert_after=insert_after, insert_first=insert_first, insert_last=insert_last, insert_at=insert_at)
        return string_id
    
    def remove_string_conn(self, string):
        string_id = self.get_id(string)
        if string_id in self.rev_string_parents:
            parent_id = self.rev_string_parents[string_id]
            self.string_parents[parent_id].remove(string_id)
            del self.rev_string_parents[string_id]
    
    def remove_string(self, string):
        string_id = self.get_id(string)
        self.remove_string_conn(string_id)
        if string_id in strings:
            del strings[string_id]
    
    def add_sentence(self, sentence, parent=None, children=[], strings=[], insert_before=None, insert_after=None, insert_first=False, insert_last=True, insert_at=None):
        node = Node(node_id=None, tag="sentence", loc=None)
        node_id = self.add_node(node, parent=parent, children=children, strings=strings, insert_before=insert_before, insert_after=insert_after, insert_first=insert_first, insert_last=insert_last, insert_at=insert_at)
        sentence.id = node_id
        self.sentences[node_id] = sentence.copy()
        return node_id
    
    def remove_node(self, node, reconnect=False, recursive=False):
        node_id = self.get_id(node)
        
        if node_id in self.nodes:
            del self.nodes[node_id]
        if node_id in self.sentences:
            del self.sentences[node_id]
        
        if reconnect:
            # record old nod connections
            original_parent = self.get_parent(node_id)
            original_children = self.get_children(node_id)
            original_strings = self.get_node_strings(node_id)
            original_pos = self.get_sibling_pos(node_id)
            
            # delete node
            self.disconnect_node(node_id)
            if node_id in self.string_parents:
                del self.string_parents[node_id]
            for string in original_strings:
                self.remove_string(self, string)
                
            # add connections between original parent and children
            for child in reversed(original_children):  # reversed to preserve order and position of children
                self.add_conn(original_parent, child, insert_at=original_pos)
            for string in reversed(original_strings):  # reversed to preserve order and position of children
                self.add_string_conn(original_parent, string, insert_at=original_pos)
            return
        
        if recursive:
            original_children = self.get_children(node_id)
            for child in original_children:
                self.remove_node(child, reconnect=reconnect, recursive=recursive)
            self.disconnect_node(node_id)
            if node_id in self.string_parents:
                del self.string_parents[node_id]
            for string in self.get_node_strings(node_id):
                self.remove_string(self, string)
            return
        
        # if not reconnect or recursive
        self.disconnect_node(node_id)
        if node_id in self.string_parents:
            for string in self.get_node_strings(node_id):
                self.remove_string(self, string)
            del self.string_parents[node_id]
                
    def replace_node(self, source, target, keep_id=True):
        source_id = self.get_id(source)
        source_parent = self.get_parent(source)
        source_children = self.get_children(source)
        source_pos = self.get_sibling_pos(source)
        self.remove_node(source)
        
        if keep_id:
            target_id = source_id
        else:
            target_id = self.new_node_id()
        target.id = target_id
        self.add_node(target, parent=source_parent, children=source_children, insert_at=source_pos)
        
        return target_id

    def move_node(self, node, new_parent, insert_before=None, insert_after=None, insert_first=False, insert_last=True, insert_at=None):
        self.remove_conn_parent(node)
        self.add_conn(new_parent, node, insert_before=insert_before, insert_after=insert_after, insert_first=insert_first, insert_last=insert_last, insert_at=insert_at)
        
    def update_locs(self):
        roots = self.get_roots()
        for i,root in enumerate(sorted(roots)):
            self._rec_update_locs(root, [i])

        for k,v in self.strings.items():
            parent = self.rev_string_parents[k]
            siblings = self.string_parents[parent]
            n_i = siblings.index(k)
            self.strings[k].loc = tuple(list(self.nodes[parent].loc) + [n_i])
            
        for k,v in self.sentences.items():
            v.loc = self.nodes[k].loc

    def _rec_update_locs(self, node_id, loc):
        if node_id in self.nodes: self.nodes[node_id].loc = tuple(loc)
        if node_id in self.hier:
            for i, child_id in enumerate(self.hier[node_id]):
                self._rec_update_locs(child_id, loc+[i])
    
    def renumber(self):
        new_doc = self.copy()
        
        node_map = {}
        for i,(n_id, node) in enumerate(sorted(self.nodes.items(), key=lambda x:x[1].loc)):  # sort by loc
            node_map[n_id] = i
        
        new_doc.rev_hier = {}
        for k,v in self.rev_hier.items():
            new_doc.rev_hier[node_map[k]] = node_map[v]
        new_doc.hier = {}
        for k,vs in self.hier.items():
            new_doc.hier[node_map[k]] = [node_map[v] for v in vs]
        new_doc.nodes = {}
        for k,v in self.nodes.items():
            v.id = node_map[k]
            new_doc.nodes[v.id] = v.copy()
        new_doc.sentences = {}
        for k,v in self.sentences.items():
            v.id = node_map[k]
            new_doc.sentences[v.id] = v.copy()
        
        string_map = {None: None}
        for i,(s_id, node) in enumerate(sorted(self.strings.items(), key=lambda x:x[1].loc)):  # sort by loc
            string_map[s_id] = i
        
        new_doc.rev_string_parents = {}
        for k,v in self.rev_string_parents.items():
            new_doc.rev_string_parents[string_map[k]] = node_map[v]
        new_doc.string_parents = {}
        for k,vs in self.string_parents.items():
            new_doc.string_parents[node_map[k]] = [string_map[v] for v in vs]
        new_doc.strings = {}
        for k,v in self.strings.items():
            v.id = string_map[k]
            new_doc.strings[v.id] = v.copy()
            
        self.rev_hier = new_doc.rev_hier
        self.hier = new_doc.hier
        self.nodes = new_doc.nodes
        self.sentences = new_doc.sentences
        self.strings = new_doc.strings
        self.string_parents = new_doc.string_parents
        self.rev_string_parents = new_doc.rev_string_parents
        
    
class Node():
    def __init__(self, node_id, tag, loc):
        self.id = node_id
        self.tag = tag
        self.loc = loc
    
    def get_tag(self):
        if self.tag.split('-')[0] == 'content': 
            return 'content'
        else:
            return self.tag
    
    def copy(self):
        return Node(copy.copy(self.id), copy.copy(self.tag), copy.copy(self.loc))
        
class String(Node):
    def __init__(self, string_id, string, loc):
        super().__init__(node_id=string_id, tag="string", loc=loc)
        self.string = string
        
    def copy(self):
        return String(copy.copy(self.id), copy.copy(self.string), copy.copy(self.loc))

class Sentence(Node):
    def __init__(self, sentence_id, string, loc, conn_sentence=None, char_loc=None):
        super().__init__(node_id=sentence_id, tag="sentence", loc=loc)
        self.string = string
        self.conn_sentence = conn_sentence
        self.char_loc = char_loc
    
    def copy(self):
        return Sentence(copy.copy(self.id), copy.copy(self.string), copy.copy(self.loc), copy.copy(self.conn_sentence), copy.copy(self.char_loc))

class Tokenizer():
    def __init__(self):
        return
    def tokenize(self,string):
        return

class SpacyTokenizer(Tokenizer):
    def __init__(self, model="en_core_web_sm"):  # 'en_core_sci_md'
        
        self.nlp = spacy.load(model)

        _quotes = spacy.lang.char_classes.CONCAT_QUOTES.replace("'", "")
        _infixes = (
            spacy.lang.char_classes.LIST_ELLIPSES
            + spacy.lang.char_classes.LIST_ICONS
            + [
                r"(?<=[{al}])\.(?=[{au}])".format(al=spacy.lang.char_classes.ALPHA_LOWER, au=spacy.lang.char_classes.ALPHA_UPPER),
                r"(?<=[{a}])[,!?](?=[{a}])".format(a=spacy.lang.char_classes.ALPHA),
                r'(?<=[{a}])[:<>=](?=[{a}])'.format(a=spacy.lang.char_classes.ALPHA),
                r"(?<=[{a}]),(?=[{a}])".format(a=spacy.lang.char_classes.ALPHA),
                r"(?<=[{a}])([{q}\)\]\(\[])(?=[{a}])".format(a=spacy.lang.char_classes.ALPHA, q=_quotes),
                r"(?<=[{a}])--(?=[{a}])".format(a=spacy.lang.char_classes.ALPHA),
                r"(?<=[{a}])-(?=[{a}])".format(a=spacy.lang.char_classes.ALPHA),  # add rule splitting words by hyphen
                r"(?<=[{a}])/(?=[{a}])".format(a=spacy.lang.char_classes.ALPHA),  # add rule splitting words by slash
                r"(?<=[0-9])-(?=[0-9])",
            ]
        )
        infix_re = spacy.util.compile_infix_regex(_infixes)

        self.nlp.tokenizer = spacy.tokenizer.Tokenizer(self.nlp.vocab,
                                                       prefix_search=self.nlp.tokenizer.prefix_search,
                                                       suffix_search=self.nlp.tokenizer.suffix_search,
                                                       infix_finditer=infix_re.finditer,
                                                       token_match=self.nlp.tokenizer.token_match,
                                                       rules=self.nlp.Defaults.tokenizer_exceptions)
        
    def tokenize(self, string, split_sentence=True):
        doc = self.nlp(string)
        tokens = [Token(string=str(w), pos=str(w.pos_), start_idx=int(w.idx), end_idx=int(w.idx)+len(w), lemma=None, tag=str(w.tag_), dep=str(w.dep_)) for w in doc]
            
        if split_sentence:
            sentences = []
            sentence_locs = []
            for sent in doc.sents:
                start_idx = int(tokens[sent.start].start_idx)
                try: end_idx = int(tokens[sent.end-1].end_idx)
                except: end_idx = len(string)

                for i in range(sent.start,sent.end):
                    tokens[i].start_idx -= start_idx
                    tokens[i].end_idx -= start_idx

                sentences.append(ConnectedSentence(tokens=tokens[sent.start:sent.end], string=string[start_idx:end_idx], conn={}))
                sentence_locs.append((start_idx,end_idx))
        else:
            sentences = [ConnectedSentence(tokens=tokens, string=string)]
            sentence_locs = [(0,len(string))]

        return sentences, sentence_locs

        
class Lemmatizer():
    def __init__(self):
        return
    def lemmatize(self,string):
        return
    def lemmatize_token(self,token):
        return
        
class SpacyLemmatizer(Lemmatizer):
    def __init__(self):
        self.lemmatizer = spacy.lang.en.English.Defaults.create_lemmatizer()
        # lemmatizer = Lemmatizer(spacy.lang.en.LEMMA_INDEX, spacy.lang.en.LEMMA_EXC, spacy.lang.en.LEMMA_RULES)
        
    def lemmatize(self,string,pos):
        return str(self.lemmatizer(string.lower(), pos)[0])
        
        
class Token():
    def __init__(self, string, pos, start_idx, end_idx, tag=None, **kwargs):
        self.string = string
        self.pos = pos
        self.start_idx = start_idx
        self.end_idx = end_idx
        self.tag = tag
        self.properties = dict(kwargs)
        
    def copy(self):
        return Token(copy.copy(self.string), 
                     copy.copy(self.pos), 
                     copy.copy(self.start_idx), 
                     copy.copy(self.end_idx), 
                     copy.copy(self.tag), 
                     **copy.deepcopy(self.properties))
    
    def __iter__(self):
        d = {   'string': self.string,
                'pos': self.pos,
                'start_idx': self.start_idx,
                'end_idx': self.end_idx,
                'tag': self.tag,
                'properties': self.properties}
        
        for k,v in d.items():
            yield (k,v)
    
    def __repr__(self):
        return repr(dict(self))
    
    @staticmethod
    def loads(s):
        return Token.from_dict(eval(s))
        
    def dumps(self):
        return repr(self)
    
    @staticmethod
    def from_dict(d):
        return Token(**d)
    

class ConnectedSentence():
    # stores connected sentence data
    # handles manipulation of this data (ie adding and removing connections/tokens)
    def __init__(self, tokens, string, conn=None, rev_conn=None, hier=None):
        if isinstance(tokens,list):
            self.tokens = {i:t for i,t in enumerate(tokens)}
        else:
            if isinstance(tokens,dict):
                self.tokens = tokens
            else:
                raise TypeError(f"tokens must be list or dict, not {type(tokens)}")
                   
        self.string = string
        
        if not conn:
            ids = sorted(list(self.tokens.keys()))
            if len(ids):
                conn = {None: {ids[0]}}
                j=0
                for j in range(1,len(ids)):
                    try: conn[ids[j-1]].add(ids[j])
                    except: conn[ids[j-1]] = {ids[j]}
                else: conn[ids[j]] = {None}
            else:
                conn = {}
        self.conn = conn
        
        if not rev_conn:
            rev_conn = self._gen_rev_conn(self.conn)
        self.rev_conn = rev_conn          
        
        if not hier:
            hier = {}
        self.hier = hier
    
    def copy(self):
        return ConnectedSentence({i:t.copy() for i,t in self.tokens.items()}, copy.copy(self.string), copy.deepcopy(self.conn))
    
    def _gen_rev_conn(self,conn):
        rev_conn = {}
        for k,vs in conn.items():
            for v in vs:
                try: rev_conn[v].add(k)
                except: rev_conn[v] = {k}
        return rev_conn               
    
    def add_token(self,token):
        i = max(self.tokens.keys()) + 1
        self.tokens[i] = token
        return i
    
    def add_conn(self,source,target):
        if source in self.conn:
            self.conn[source].add(target)
        else:
            self.conn[source] = {target}
        if target in self.rev_conn:
            self.rev_conn[target].add(source)
        else:
            self.rev_conn[target] = {source}
                   
    def copy_outgoing_conn(self,source,target):
        if source in self.conn:
            self.conn[target] = self.conn[source].copy()
        for t in self.conn[target]:
            if t in self.rev_conn:
                self.rev_conn[t].add(target)
            else:
                self.rev_conn[t] = {target}
        
    def copy_incoming_conn(self,source,target):
        if source in self.rev_conn:
            self.rev_conn[target] = self.rev_conn[source].copy()
        for t in self.rev_conn[target]:
            if t in self.conn:
                self.conn[t].add(target)
            else:
                self.conn[t] = {target}
                   
    def copy_conn(self,source,target):
        self.copy_outgoing_conn(source,target)
        self.copy_incoming_conn(source,target)
    
    def delete_conn(self,source,target):
        if source in self.conn:
            self.conn[source].remove(target)
        if target in self.rev_conn:
            self.conn[target].remove(source)
                   
    def delete_conn_to(self,token_i):
        if token_i in self.rev_conn:
            for t in self.rev_conn[token_i]:
                self.conn[t].remove(token_i)
            del self.rev_conn[token_i]
    
    def delete_conn_from(self,token_i):
        if token_i in self.conn:
            for t in self.conn[token_i]:
                self.rev_conn[t].remove(token_i)
            del self.conn[token_i]
                   
    def disconect(self,token_i):
        self.delete_conn_to(token_i)
        self.delete_conn_from(token_i)
    
    def next_skip_stop_words(self, token_i, stop_words):
        next_ids = set()
        if not token_i in self.conn: return set()
        for next_id in self.conn[token_i]:
            if next_id is None: 
                next_ids.add(next_id)
                continue
            if self.tokens[next_id].string.lower() in stop_words:
                next_ids.update(self.next_skip_stop_words(next_id, stop_words))
            else: next_ids.add(next_id)
        return next_ids

    def previous_skip_stop_words(self, token_i, stop_words):
        prev_ids = set()
        if not token_i in self.rev_conn: return set()
        for prev_id in self.rev_conn[token_i]:
            if prev_id is None: 
                prev_ids.add(prev_id)
                continue
            if self.tokens[prev_id].string.lower() in stop_words:
                prev_ids.update(self.previous_skip_stop_words(prev_id, stop_words))
            else: prev_ids.add(prev_id)
        return prev_ids               
                   
    def dumps(self):
        return repr(self)
    
    @staticmethod
    def loads(s):
        return ConnectedSentence.from_dict(eval(s))
    
    def __repr__(self):
        return repr(dict(self))
    
    def __iter__(self):
        d = {'tokens': {i:dict(t) for i,t in self.tokens.items()},
             'conn': self.conn,
             'rev_conn': self.rev_conn,
             'string': self.string,
             'hier': self.hier}
        
        for k,v in d.items():
            yield (k,v)
    
    @staticmethod
    def from_dict(d):
        d['tokens'] = {i:Token(t['string'], t['pos'], t['start_idx'], t['end_idx'], tag=t['tag']) for i,t in d['tokens'].items()}
        return ConnectedSentence(**d)