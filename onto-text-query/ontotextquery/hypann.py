import re
import itertools as it
from html import unescape
import copy
from lxml import etree
import sqlite3
import sqlalchemy
import requests
import json

from tqdm.auto import tqdm

import spacy
import scispacy
# from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES
# from spacy.lang.char_classes import ALPHA, ALPHA_LOWER, ALPHA_UPPER, CONCAT_QUOTES, LIST_ELLIPSES, LIST_ICONS
# from spacy.util import compile_prefix_regex, compile_infix_regex, compile_suffix_regex

import pdb

class HypothesisAnnotationHandler():
    def __init__(self):
        return

    def annotation2loc(selector, sentence_loc_index, indi_conn):
        selectors = {}
        for sel in selector:
            selectors[sel['type']] = sel
        path = selectors['RangeSelector']['startContainer']
        s_id = sentence_loc_index[path]

        char_loc = (selectors['RangeSelector']['startOffset'], selectors['RangeSelector']['endOffset'])
        token_path = get_token_path(s_id, char_loc, indi_conn)

        return s_id, char_loc, token_path

    def get_token_path(s_id, char_loc, indi_conn):
        string, sentence = get_sentence(s_id, indi_conn)
        from_beginning = set()
        before_end = set()
        for i,word in sorted(sentence['words'].items(), key=lambda x:x[0]):
            w_span = word['idx']
            if w_span[0] >= char_loc[0]:
                from_beginning.add(i)
            if w_span[1] < char_loc[1]:
                before_end.add(i)

        token_path = sorted(list(from_beginning & before_end))
        return token_path



    def request_annotations(set_id=None, limit=20):
        base_url = "https://api.hypothes.is/api"
        rows = []
        last = None
        while True:
            if set_id is None:
                r = requests.get(url=f'{base_url}/search', params={'search_after': last, 'limit': limit, 'sort': 'created', 'user': 'acct:timrozday@hypothes.is', 'wildcard_url': 'http://0.0.0.0:8000/'}, headers={'Authorization': 'Bearer 6879-l91CDYdk-OqBW0ZfhMg8t0QGH_DdH1RcTmT5LlNx2bk'})
            else:
                r = requests.get(url=f'{base_url}/search', params={'search_after': last, 'limit': limit, 'sort': 'created', 'user': 'acct:timrozday@hypothes.is', 'uri': f'http://0.0.0.0:8000/{set_id}.htm'}, headers={'Authorization': 'Bearer 6879-l91CDYdk-OqBW0ZfhMg8t0QGH_DdH1RcTmT5LlNx2bk'})
            r = r.json()
            rows += r['rows']
            last = rows[-1]['created']
            if len(r['rows']) < limit: break

        return rows



    def save_h_annotations(h_conn, indi_conn, set_id=None):
        set_id2spl_id_cache = {}
        sentence_loc_cache = {}
        for row in request_annotations(set_id=set_id, limit=50):
            set_id = row['uri'].split('/')[-1].split('.')[0]  # get set_id from row
            if not set_id in set_id2spl_id_cache:
                set_id2spl_id_cache[set_id] = get_spl_id(set_id, indi_conn)
            spl_id = set_id2spl_id_cache[set_id]

            if not spl_id in sentence_loc_cache:
                sentence_loc_cache[spl_id] = gen_sentence_loc_index(spl_id, indi_conn)
            sentence_loc_index = sentence_loc_cache[spl_id]

            s_id, char_loc, token_path = annotation2loc(row['target'][0]['selector'], sentence_loc_index, indi_conn)
            tags = set(row['tags'])
            data = json.dumps(row)

            c = h_conn.execute('insert into annotation(h_id,set_id,sentence_id,char_range,uri,link,user,created,updated,data,spl_id,token_path) values (?,?,?,?,?,?,?,?,?,?,?,?) \
                                on conflict(h_id) do update set h_id=?,set_id=?,sentence_id=?,char_range=?,uri=?,link=?,user=?,created=?,updated=?,data=?,spl_id=?, token_path=?', \
                               (row['id'],set_id,s_id,repr(char_loc),row['uri'],row['links']['html'],row['user'],row['created'],row['updated'],data,spl_id,repr(token_path), \
                                row['id'],set_id,s_id,repr(char_loc),row['uri'],row['links']['html'],row['user'],row['created'],row['updated'],data,spl_id,repr(token_path)))
            a_id = int(c.lastrowid)

            for t in tags:
                try:
                    c = h_conn.execute('insert into tag(tag) values (?)', (t,))
                    t_id = int(c.lastrowid)
                except: t_id = list(h_conn.execute('select id from tag where tag=?',(t,)))[0][0]
                try: h_conn.execute('insert into annotation2tag(a_id,t_id) values (?,?)',(a_id,t_id))
                except: pass

        h_conn.commit()



    def gen_node_hier_cached(spl_id, conn, cache):
        if not spl_id in cache:
            cache[spl_id] = gen_node_hier(spl_id, conn)
        return cache[spl_id], cache

    def create_hypothesis_annotation(s_id, token_path, tags, indi_conn, node_hier_cache, group_id='vk6XDNo7'):
        string, sentence = get_sentence(s_id, indi_conn)
        set_id = list(indi_conn.execute('select set_id from sentences left join spl on sentences.spl_id=spl.id where sentences.id=?', (s_id,)))[0][0]

        # generate idx span
        start_idx = sentence['words'][token_path[0]]['idx'][0]
        end_idx = sentence['words'][token_path[-1]]['idx'][-1]
        if string[end_idx+1] == ' ': end_idx-=1  # remove trailing space
        text = string[start_idx:end_idx]
        l = 32
        prefix = string[start_idx-1-l:start_idx-1]
        suffix = string[end_idx+1:end_idx+1+l]

        # get sentence path in html document
        (node_hier, rev_node_hier, node_tags, sentence_parents, rev_sentence_parents), node_hier_cache = gen_node_hier_cached(spl_id, indi_conn, node_hier_cache)
        path = sentence2annotation_loc(s_id, node_hier, rev_node_hier, node_tags, sentence_parents, rev_sentence_parents)
        path_str = '/div[1]/' + '/'.join([f'{tag}[{n}]' for tag,n in path])

        uri = f"http://0.0.0.0:8000/{set_id}.htm"

        annotation = {
            "uri": uri,
            "text": text,
            "tags": list(tags),
            "group": group_id,
            "target": [{
                'selector': [
                    {
                        "startContainer": path_str,
                        "endContainer": path_str,
                        'startOffset': start_idx,
                        'endOffset': end_idx,
                        'type': 'RangeSelector',
                    },
                    {
                        'exact': text,
                        'prefix': prefix,
                        'suffix': suffix,
                        'type': 'TextQuoteSelector'
                    }
                ],
                'source': uri
            }]
        }

        return annotation, node_hier_cache



    def post_annotation(annotation):
        base_url = "https://api.hypothes.is/api"

        headers = {
            'Authorization': 'Bearer 6879-l91CDYdk-OqBW0ZfhMg8t0QGH_DdH1RcTmT5LlNx2bk', 
            'Content-type': 'application/json'
        }

        r = requests.post(f'{base_url}/annotations', json=annotation, headers=headers)
        return r.status_code