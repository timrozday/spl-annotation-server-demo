import re
import itertools as it
from html import unescape
import copy
from lxml import etree
import sqlite3
import sqlalchemy
import requests
import json

from tqdm.auto import tqdm

import spacy
import scispacy
# from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES
# from spacy.lang.char_classes import ALPHA, ALPHA_LOWER, ALPHA_UPPER, CONCAT_QUOTES, LIST_ELLIPSES, LIST_ICONS
# from spacy.util import compile_prefix_regex, compile_infix_regex, compile_suffix_regex

import pdb