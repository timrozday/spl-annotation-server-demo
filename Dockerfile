FROM nginx:latest
LABEL Author="Tim Rozday" Email="timrozday@ebi.ac.uk"
LABEL Description="Webserver for DailyMed SPL annotation of indications" Date="22Jul2020"

# Set up environmental vairables
ENV FLASK_APP /home/enduser/webservice/flask-server.py

# Copy code and data across
ADD ./webservice /home/enduser/webservice
# ADD ./data /home/enduser/data
ADD ./onto-text-query /home/enduser/onto-text-query
RUN chmod a+rwx /home/enduser/webservice/run.sh
RUN mkdir /home/enduser/logs
RUN chmod a+rwx /home/enduser/logs

# Install dependencies
RUN apt-get -y update && \
    apt-get -y install python3 python3-pip vim
RUN pip3 install flask requests gunicorn fuzzywuzzy tqdm lxml python-Levenshtein sqlalchemy spacy scispacy /home/enduser/onto-text-query

# Set to non-root user
RUN useradd -ms /bin/bash enduser
USER enduser

# Run flask
EXPOSE 5000
WORKDIR /home/enduser/webservice
CMD ["./run.sh"]